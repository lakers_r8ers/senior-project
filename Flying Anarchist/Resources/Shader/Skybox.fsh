varying lowp vec4 colorVarying;
varying lowp vec2 tex_coord;

uniform sampler2D s2d_texture;

void main()
{
    gl_FragColor = texture2D(s2d_texture, tex_coord);
}