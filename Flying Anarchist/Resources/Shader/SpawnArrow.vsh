attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUV;

varying vec2 tex_coord;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;

void main()
{
    tex_coord = aUV;
    
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(aPosition, 1.0);
}