varying lowp vec3 colorVarying;

void main()
{
    gl_FragColor = vec4(colorVarying, 1.0);
}