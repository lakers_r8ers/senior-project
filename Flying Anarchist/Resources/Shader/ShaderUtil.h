//
//  ShaderUtil.h
//  Level Editor
//
//  Created by Lion User on 06/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "ShaderManager.h"
#import "TextureManager.h"
#import <vector>

@interface ShaderUtil : NSObject
{
    GLuint programHandle;
    
    NSMutableDictionary *attributeHandles;
    NSMutableDictionary *uniformHandles;
}

@property (nonatomic, readonly) GLuint programHandle;

- (id) initWithShaderSettings:(NSDictionary *)settings;
- (void)useShader;
- (int)uniformLocation:(NSString *)name;
- (GLuint) attributeHandle:(NSString *)name;
- (void)bindAttribute:(NSString *)name
           withSize:(GLuint)size;
- (void)disableAttribute:(NSString *)name;
- (void)printProgramInfo;



@end
