attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUV;

varying vec2 tex_coord;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform float u;
uniform float v;


void main()
{
    //tex_coord = vec2(aUV.x / 4.0 + u,1.0-aUV.y / 4.0 + 1.0-v);
    tex_coord = aUV;
    
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(aPosition, 1.0);
}