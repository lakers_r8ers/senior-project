//
//  ShaderManager.m
//  Level Editor
//
//  Created by Lion User on 06/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "ShaderManager.h"

@implementation ShaderManager

static ShaderManager *sharedShaderManager;

#pragma mark - Singleton 

+ (ShaderManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedShaderManager = [[ShaderManager alloc] init];
    }
    return sharedShaderManager;
}

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        shaderPrograms = [[NSMutableDictionary alloc] init];
        activeProgram = 0;
    }
    return self;
}

#pragma mark - Shader management

- (NSArray *)allShaders
{
    return [shaderPrograms allValues];
}

- (ShaderUtil *)currentShader
{
    return currentShader;
}

- (ShaderUtil *)useShader:(NSString *)name
{
    currentShader = [self shaderForName:name];
    if (activeProgram != currentShader.programHandle)
    {
        glUseProgram(currentShader.programHandle);
        activeProgram = currentShader.programHandle;
    }
    
    return currentShader;
}


- (void)useShaderObject:(ShaderUtil *)shader
{
    currentShader = shader;
    if (activeProgram != currentShader.programHandle)
    {
        glUseProgram(currentShader.programHandle);
        activeProgram = currentShader.programHandle;
    }
}

- (ShaderUtil *)shaderForName:(NSString *)name
{
    return [shaderPrograms objectForKey:name];
}


- (ShaderUtil *)createShader:(NSString *)name withSettings:(NSDictionary *)settings
{
    ShaderUtil *shader = [[ShaderUtil alloc] initWithShaderSettings:settings];
    
    [shaderPrograms setObject:shader forKey:name];
    
    return shader;
}

- (ShaderUtil *)createShader:(NSString *)name withFile:(NSString *)file
{
    ShaderUtil *existingShader = [self shaderForName:name];
    if (existingShader != nil)
    {
        return existingShader;
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:
                          [[NSBundle mainBundle] pathForResource:file
                                                          ofType:@""]];
    return [self createShader:name withSettings:dict];
}

- (void)removAllShaders
{
    [shaderPrograms removeAllObjects];
}

- (void)removeShader:(NSString *)name
{
    ShaderUtil *shader = [self shaderForName:name];
    
    if (shader)
    {
        if (shader.programHandle == activeProgram)
        {
            glUseProgram(0);
        }
        
        [shaderPrograms removeObjectForKey:name];
    }
}

- (void)disableCurrentShader
{
    glUseProgram(0);
    activeProgram = 0;
}

@end




