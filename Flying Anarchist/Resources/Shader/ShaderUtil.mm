//
//  ShaderUtil.m
//  Level Editor
//
//  Created by Lion User on 06/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "ShaderUtil.h"

@implementation ShaderUtil

@synthesize programHandle;


#pragma mark - Shader compiling and linking

- (BOOL)compileShader:(GLuint *)shader file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    GLenum type = GL_VERTEX_SHADER;
    
    NSString *extension = [file pathExtension];

    if ([extension isEqualToString:@"vsh"])
    {
        type = GL_VERTEX_SHADER;
    }
    else if([extension isEqualToString:@"fsh"])
    {
        type = GL_FRAGMENT_SHADER;
    }
    else
    {
        NSLog(@"Unknown shader file extension");
        return FALSE;
    }
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file
                         encoding:NSUTF8StringEncoding error:nil] UTF8String];
    
    if (!source)
    {
        NSLog(@"Failed to load vertex shader");
        return FALSE;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
    [self printLog:*shader];
    
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        glDeleteShader(*shader);
        return FALSE;
    }
    
    return TRUE;
}


- (BOOL)linkProgram:(GLuint)prog
{
    GLint status, logLength;
    
    glLinkProgram(prog);
    
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    
    [self printLog:prog];
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0)
    {
        return FALSE;
    }
    
    return TRUE;
}

#pragma mark - Initialization

- (id)initWithShaderSettings:(NSDictionary *)settings
{
    self = [super init];
    if (self)
    {
        NSArray *shaderFiles = [[NSArray alloc] initWithArray:[settings objectForKey:@"Files"]];
        NSArray *attributes = [[NSArray alloc] initWithArray:[settings objectForKey:@"Attributes"]];
        NSArray *uniforms = [[NSArray alloc] initWithArray:[settings objectForKey:@"Uniforms"]];
    
        attributeHandles = [[NSMutableDictionary alloc] init];
        uniformHandles = [[NSMutableDictionary alloc] init];

        programHandle = glCreateProgram();
        
        NSMutableArray *shaderList = [[NSMutableArray alloc] init];
        for (NSString *file in shaderFiles)
        {
            GLuint shader;
            
            if (![self compileShader:&shader
                                file:[[NSBundle mainBundle]
                     pathForResource:file ofType:nil]])
            {
                NSLog(@"Failed to compile shader: %@", file);
            }
            
            [shaderList addObject:[NSNumber numberWithInt:shader]];
        }
        
        for (NSNumber *shader in shaderList)
        {
            //Attach each shader to the program
            glAttachShader(programHandle, [shader unsignedIntValue]);
        }
        
        GLuint attribLoc = 0;
        for (NSString *attribute in attributes)
        {
            //bind attributes to program
            glBindAttribLocation(programHandle, attribLoc, [attribute UTF8String]);
            [attributeHandles setObject:[NSNumber numberWithInt:attribLoc]
                                 forKey:attribute];
            attribLoc++;
        }
        
        if (![self linkProgram:programHandle])
        {
            NSLog(@"Failed to link program: %d", programHandle);
            
            for (NSNumber *shader in shaderList)
            {
                if ([shader unsignedIntValue])
                {
                    glDeleteShader([shader unsignedIntValue]);
                }
            }
            if (programHandle)
            {
                glDeleteShader(programHandle);
                programHandle = 0;
            }
            
        }
        if (programHandle)
        {
            for (NSString *uniform in uniforms)
            {
                int uniformHandle = glGetUniformLocation(programHandle, [uniform UTF8String]);
                if (uniformHandle < 0)
                {
                    NSLog(@"Error: glGetUniformLocation returns negative value");
                }
                
                [uniformHandles setObject:[NSNumber numberWithInt:uniformHandle]
                                   forKey:uniform];
            }
        }
    }
    return self;
}

#pragma mark - Using the shader

- (void)useShader
{
    [[ShaderManager sharedManager] useShaderObject:self];
}

#pragma mark - Access to uniforms and attributes

- (int)uniformLocation:(NSString *)name
{
    return [[uniformHandles objectForKey: name] intValue];
}

- (GLuint)attributeHandle:(NSString *)name
{
    return [[attributeHandles objectForKey:name] unsignedIntValue];
}

- (void)bindAttribute:(NSString *)name
           withSize:(GLuint)size
{
    GLuint attribHandle = [self attributeHandle:name];

    glEnableVertexAttribArray(attribHandle);
    glVertexAttribPointer(attribHandle, size, GL_FLOAT, GL_FALSE, 0, 0);
}

- (void)disableAttribute:(NSString *)name
{
    glDisableVertexAttribArray([self attributeHandle:name]);
}

#pragma mark - Shader Debug Printing

- (void) printLog:(GLuint)obj
{
    int infoLogLength = 0;
    
    if (glIsProgram(obj))
        glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &infoLogLength);
    else
        glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &infoLogLength);
    
    if (infoLogLength)
    {
        char infoLog[infoLogLength];
        
        if (glIsProgram(obj))
            glGetProgramInfoLog(obj, infoLogLength, &infoLogLength, infoLog);
        else
            glGetShaderInfoLog(obj, infoLogLength, &infoLogLength, infoLog);
        
        NSLog(@"%s\n",infoLog);
    }
}

- (void)printActiveAttributeInfo
{
    //Active Attributes
    char *name;
    GLint activeAttribs = 0, max_length = 0;
    glGetProgramiv(programHandle, GL_ACTIVE_ATTRIBUTES, &activeAttribs);
    glGetProgramiv(programHandle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH,
                   &max_length);
    if (activeAttribs)
    {
        name = (char *) malloc(max_length + 1);
        printf("Attribute Info:\n");
        for (unsigned i = 0; i < activeAttribs; i++)
        {
            GLint size;
            GLenum type;
            
            glGetActiveAttrib(programHandle, i, max_length+1, NULL,
                              &size, &type, name);
            printf("\t%s is at location %d\n", name,
                   glGetAttribLocation(programHandle, name));
        }
        free(name);
    }
    else
        printf("No active attributes\n");
}

- (void)printActiveUniformInfo
{
    //Active Uniforms
    char *name;
    GLint activeUniforms = 0, max_length = 0;
    
    glGetProgramiv(programHandle, GL_ACTIVE_UNIFORMS, &activeUniforms);
    glGetProgramiv(programHandle, GL_ACTIVE_UNIFORM_MAX_LENGTH,
                   &max_length);
    if (activeUniforms)
    {
        name = (char *) malloc(max_length + 1);
        printf("Uniform Info:\n");
        for (unsigned i = 0; i < activeUniforms; i++)
        {
            GLint size;
            GLenum type;
            
            glGetActiveUniform(programHandle, i, max_length, NULL,
                              &size, &type, name);
            printf("\t%s is at location %d\n", name,
                   glGetUniformLocation(programHandle, name));
        }
    }
    else
        printf("No active uniforms\n");
}

- (void)printAttachedShadersInfo
{
    //Attached Shaders
    GLsizei size;
    glGetProgramiv(programHandle, GL_ATTACHED_SHADERS, &size);
    
    if (size)
        printf("Attached shaders: %d\n", size);
    else
        printf("No attached shaders\n");
}

- (void)printProgramInfo
{
    printf("Shader Program Info: Program %d\n", programHandle);
    [self printAttachedShadersInfo];
    [self printActiveAttributeInfo];
    [self printActiveUniformInfo];
}

@end