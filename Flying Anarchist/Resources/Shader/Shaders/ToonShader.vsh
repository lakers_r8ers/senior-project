//
//  Shader.vsh
//  Flying Anarchist
//
//  Created by Lion User on 26/12/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUV;

uniform vec3 camPos;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

varying vec2 tex_coord;
varying vec3 lightPosition;
varying vec3 eyeNormal;
varying vec3 position;

void main()
{
    eyeNormal = normalize(normalMatrix * aNormal);
    lightPosition = camPos;
    tex_coord = aUV;
    position = aPosition;
    
    //float nDotVP = max(0.0, dot(eyeNormal, normalize(lightPosition)));
    //colorVarying = diffuseColor * nDotVP;
    
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(aPosition, 1.0);
}
