//
//  Shader.fsh
//  Flying Anarchist
//
//  Created by Lion User on 26/12/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

varying lowp vec3 eyeNormal;
varying lowp vec2 tex_coord;
varying lowp vec3 lightPosition;
varying lowp vec3 position;

uniform sampler2D s2d_texture;

void main()
{
    lowp vec4 color = texture2D(s2d_texture, tex_coord);
    lowp vec3 viewDir = normalize(lightPosition - position);
    lowp vec3 lightDir;
    //lowp float attenuation;
    
    lowp vec3 vertexToLightSource = vec3(lightPosition - position);
    lowp float distance = length(vertexToLightSource);
    //attenuation = 1.0 / distance;
    lightDir = normalize(vertexToLightSource);
    
    lowp vec3 fragmentedColor = vec3(color) / vec3(2.0);
    
    lowp float illum = max(0.0, dot(eyeNormal, lightDir));
    if (illum >= 0.0 && illum <= 1.0)
    {
        fragmentedColor = vec3(1.0, 1.0, 1.0) * vec3(color);
    }
    
    gl_FragColor = vec4(fragmentedColor, 1.0);
}
