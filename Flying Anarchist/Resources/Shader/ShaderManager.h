//
//  ShaderManager.h
//  Level Editor
//
//  Created by Lion User on 06/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <GLKit/GLkit.h>
#import "ShaderUtil.h"

@class ShaderUtil;
@interface ShaderManager : NSObject
{
    NSMutableDictionary *shaderPrograms;
    GLuint activeProgram;
    ShaderUtil *currentShader;
}

+ (ShaderManager *)sharedManager;

- (NSArray *)allShaders;
- (ShaderUtil *)currentShader;
- (ShaderUtil *)useShader:(NSString*)name;

- (ShaderUtil *)createShader:(NSString *)name withSettings:(NSDictionary *)settings;
- (ShaderUtil *)createShader:(NSString *)name withFile:(NSString *)file;

- (void)useShaderObject:(ShaderUtil *)shader;

- (void)removeShader:(NSString *)name;
- (void)removAllShaders;
- (void)disableCurrentShader;
@end
