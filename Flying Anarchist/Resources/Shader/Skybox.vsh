attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aUV;

varying vec4 colorVarying;
varying vec2 tex_coord;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

void main()
{
    vec3 eyeNormal = normalize(normalMatrix * aNormal);
    vec3 lightPosition = vec3(0.0, 0.0, 1.0);
    vec4 diffuseColor = vec4(0.0, 0.5, 0.0, 1.0);
    
    float nDotVP = max(0.0, dot(eyeNormal, normalize(lightPosition)));
    
    //colorVarying = diffuseColor * nDotVP;
    colorVarying = vec4(0.0, 0.5, 0.0, 1.0);
    
    tex_coord = aUV;
    
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(aPosition, 1.0);
}