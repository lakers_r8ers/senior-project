attribute vec3 aPosition;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform vec4 uColor;

varying vec4 colorVarying;

void main()
{
    colorVarying = uColor;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(aPosition, 1.0);
}