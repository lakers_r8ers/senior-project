//
//  MeshUtil.m
//  Level Editor
//
//  Created by Lion User on 24/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "MeshUtil.h"

@implementation MeshUtil

#pragma markk -- Mesh Initializations

- (id)initMesh:(NSString *)name
{
    if (self = [super init])
    {
        [self loadMesh:name];
    }
    return self;
}

#pragma mark -- Mesh resizing functions

- (void)centerMeshByExtentsWithCenter:(glm::vec3)centerLocation
{
    if (tempVertices.size() < 2)
        return;
    
    glm::vec3 min, max;
    {
        std::vector<glm::vec3>::const_iterator it = tempVertices.begin();
        min = max = *it;
        for (; it != tempVertices.end(); ++it)
        {
            if (min.x > it->x)
                min.x = it->x;
            if (min.y > it->y)
                min.y = it->y;
            if (min.z > it->z)
                min.z = it->z;
            
            if (max.x < it->x)
                max.x = it->x;
            if (max.y < it->y)
                max.y = it->y;
            if (max.z < it->z)
                max.z = it->z;
        }
    }
    glm::vec3 center = (max + min) / 2.f;
    
    glm::vec3 vertexOffset = centerLocation - center;
    for (int i = 0; i < tempVertices.size(); i++)
        tempVertices[i] += vertexOffset;
}

- (void)resizeMesh:(glm::vec3)scale
{
    if (tempVertices.size() < 2)
        return;
    
    glm::vec3 min, max;
    {
        std::vector<glm::vec3>::const_iterator it = tempVertices.begin();
        min = max = *it;
        for (; it != tempVertices.end(); ++it)
        {
            if (min.x > it->x)
                min.x = it->x;
            if (min.y > it->y)
                min.y = it->y;
            if (min.z > it->z)
                min.z = it->z;
            
            if (max.x < it->x)
                max.x = it->x;
            if (max.y < it->y)
                max.y = it->y;
            if (max.z < it->z)
                max.z = it->z;
        }
    }
    
    glm::vec3 extent = max - min;
    glm::vec3 resize = scale / std::max(extent.x,
                               std::max(extent.y,
                                        extent.z));
    
    for (int i = 0; i < tempVertices.size(); i++)
        tempVertices[i] *= resize;
}

#pragma mark -- Mesh Loading

- (void)loadMesh:(NSString *)name
{
    FILE *file = fopen(name.UTF8String, "r");
    char str[30];
    int resized = 0;
    
    if (file != NULL)
    {
        while (fgets(str, 30, file) != NULL)
        {
            if (strncmp("v ", str, 2) == 0)
            {
                glm::vec3 pos;
                sscanf(str, "v %f %f %f", &pos.x, &pos.y, &pos.z);
                tempVertices.push_back(pos);
                hasVertex = true;
            }
            else if (strncmp("vt", str, 2) == 0)
            {
                glm::vec2 uv;
                sscanf(str, "vt %f %f", &uv.x, &uv.y);
                tempUvs.push_back(glm::vec2(uv.x, 1 - uv.y));
                hasTexture = true;
            }
            else if (strncmp("vn", str, 2) == 0)
            {
                glm::vec3 norm;
                sscanf(str, "vn %f %f %f", &norm.x, &norm.y, &norm.z);
                tempNormals.push_back(norm);
                hasNormal = true;
            }
            else if (strncmp("f", str, 1) == 0)
            {
                glm::vec3 pos, uv, norm;
                
                if (!resized)
                {
                    resized++;
                    //scale the mesh appropriately
                    [self resizeMesh:glm::vec3(1.f)];
                    //center mesh to camera view
                    [self centerMeshByExtentsWithCenter:glm::vec3(0.f)];
                }
                
                if (hasVertex && hasNormal && hasTexture)
                {
                    sscanf(str, "f %f/%f/%f %f/%f/%f %f/%f/%f",
                       &pos.x, &uv.x, &norm.x,
                       &pos.y, &uv.y, &norm.y,
                       &pos.z, &uv.z, &norm.z);
                }
                else if (hasVertex && hasNormal && (!hasTexture))
                {
                    sscanf(str, "f %f//%f %f//%f %f//%f",
                           &pos.x, &norm.x,
                           &pos.y, &norm.y,
                           &pos.z, &norm.z);
                }

                //position triangles
                if (hasVertex)
                {
                    vertices.push_back(tempVertices[(int)(pos.x)-1]);
                    vertices.push_back(tempVertices[(int)(pos.y)-1]);
                    vertices.push_back(tempVertices[(int)(pos.z)-1]);
                }
                    
                //uvs
                if (hasTexture)
                {
                    uvs.push_back(tempUvs[(int)(uv.x)-1]);
                    uvs.push_back(tempUvs[(int)(uv.y)-1]);
                    uvs.push_back(tempUvs[(int)(uv.z)-1]);
                }
                    
                //normals
                if (hasNormal)
                {
                    normals.push_back(tempNormals[(int)(norm.x)-1]);
                    normals.push_back(tempNormals[(int)(norm.y)-1]);
                    normals.push_back(tempNormals[(int)(norm.z)-1]);
                }
            }
            else
            {
                //Ignore input
            }
        }
    }
    else
    {
        NSLog(@"MeshUtil - loadMesh: file was null");
        exit(-1);
    }
    
    verts_size = vertices.size();
    
    fclose(file);
}

- (void)loadWithShader:(ShaderUtil *)shaderOBJ;
{
    //set shader
    shader = shaderOBJ;
    
    //create VAO
    glGenVertexArraysOES(1, &vao_handle);
    glBindVertexArrayOES(vao_handle);
    
    //create VBO for vertices
    if (hasVertex)
    {
        glGenBuffers(1, &verticesVBO);
        glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3),
                 &vertices.front(), GL_STATIC_DRAW);
        [shader bindAttribute:@"aPosition" withSize:3];
    }
    
    //create VBO for normals
    if (hasNormal)
    {
        glGenBuffers(1, &normalsVBO);
        glBindBuffer(GL_ARRAY_BUFFER, normalsVBO);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3),
                 &normals.front(), GL_STATIC_DRAW);
        [shader bindAttribute:@"aNormal" withSize:3];
    }
    
    //create VBO for uvs
    if (hasTexture)
    {
        glGenBuffers(1, &uvsVBO);
        glBindBuffer(GL_ARRAY_BUFFER, uvsVBO);
        glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2),
                 &uvs.front(), GL_STATIC_DRAW);
        [shader bindAttribute:@"aUV" withSize:2];
    }
    
    glBindVertexArrayOES(0);
}

#pragma mark -- Mesh Rendering

- (void)render
{
    glBindVertexArrayOES(vao_handle);
    glDrawArrays(GL_TRIANGLES, 0, verts_size);
    [[ShaderManager sharedManager] disableCurrentShader];
    glBindVertexArrayOES(0);
}

@end
