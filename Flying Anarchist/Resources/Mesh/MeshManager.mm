//
//  MeshManger.m
//  Level Editor
//
//  Created by Lion User on 24/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "MeshManager.h"

@implementation MeshManager

static MeshManager *sharedMeshManager;

#pragma mark - Singleton

+ (MeshManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedMeshManager = [[MeshManager alloc] init];
    }
    return sharedMeshManager;
}

#pragma mark - Initialzations

- (id)init
{
    if (self = [super init])
    {
        meshList = [[NSMutableDictionary alloc] init];
        [self createMeshes];
    }
    
    return self;
}


#pragma mark - MeshManager openrations

- (void)createMeshes
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"MeshList.plist" ofType:@""];
    NSArray *meshFileList = [[NSArray alloc] initWithContentsOfFile:file];
    
    for (NSString *name in meshFileList)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:nil];
        [meshList setObject:[[MeshUtil alloc] initMesh:path] forKey:name];
    }
}

- (MeshUtil *)meshForName:(NSString *)name
{
    return [meshList objectForKey:name];
}

@end
