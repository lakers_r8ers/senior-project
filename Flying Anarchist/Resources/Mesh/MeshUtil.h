//
//  MeshUtil.h
//  Level Editor
//
//  Created by Lion User on 24/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <GLKit/GLKit.h>
#import <stdio.h>
#import <string.h>
#import "glm.hpp"
#import "type_ptr.hpp"
#import <vector>
#include "ShaderUtil.h"
#include "CheckGLErrors.h"

@interface MeshUtil : NSObject
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> uvs;
    
    std::vector<glm::vec3> tempVertices;
    std::vector<glm::vec3> tempNormals;
    std::vector<glm::vec2> tempUvs;
    
    int verts_size;
    
    GLuint verticesVBO;
    GLuint uvsVBO;
    GLuint normalsVBO;
    
    GLuint vao_handle;
    
    ShaderUtil *shader;
    
    bool hasVertex;
    bool hasNormal;
    bool hasTexture;
}

- (id)initMesh:(NSString *)name;
- (void)loadMesh:(NSString *)name;
- (void)loadWithShader:(ShaderUtil *)shaderOBJ;
- (void)render;

@end
