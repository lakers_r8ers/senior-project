//
//  MeshManger.h
//  Level Editor
//
//  Created by Lion User on 24/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "MeshUtil.h"

@class MeshUtil;
@interface MeshManager : NSObject
{
    NSMutableDictionary *meshList;
}

+ (MeshManager *)sharedManager;

- (void)createMeshes;
- (MeshUtil *)meshForName:(NSString *)name;

@end
