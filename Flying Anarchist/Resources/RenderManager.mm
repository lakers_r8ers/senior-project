//
//  RenderManager.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/27/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "RenderManager.h"

@implementation RenderManager

@synthesize context;

static RenderManager *sharedRenderManager;

#pragma mark - Singleton

+ (RenderManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedRenderManager = [[RenderManager alloc] init];
    }
    return sharedRenderManager;
}

- (id)init
{
    if (self = [super init])
    {
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    }
    return self;
}


@end
