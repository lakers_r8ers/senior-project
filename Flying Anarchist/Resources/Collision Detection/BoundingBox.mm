//
//  BoundingBox.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 3/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "BoundingBox.h"

#define NUM_VERTICES 24

enum
{
  PointA, PointB, PointC, PointD, PointE, PointF, PointG, PointH
};

@implementation BoundingBox
GLfloat cube2[] = {
    0.f, 0.f, 0.f,   0.f, 0.f, 0.f, // EF
    0.f, 0.f, 0.f,   1.f, 0.f, 0.f, // EG
    1.f, 0.f, 0.f,   1.f, 0.f, 1.f, // GH
    0.f, 0.f, 1.f,   1.f, 0.f, 1.f, // FH
    0.f, 1.f, 0.f,   0.f, 1.f, 1.f, // AB
    0.f, 1.f, 0.f,   1.f, 1.f, 0.f, // AC
    1.f, 1.f, 0.f,   1.f, 1.f, 1.f, // CD
    0.f, 1.f, 1.f,   1.f, 1.f, 1.f, // BD
    0.f, 1.f, 0.f,   0.f, 0.f, 0.f, // AE
    1.f, 1.f, 0.f,   1.f, 0.f, 0.f, // CG
    0.f, 1.f, 1.f,   0.f, 0.f, 1.f, // BF
    1.f, 1.f, 1.f,   1.f, 0.f, 1.f  // DH
};

@synthesize position, rotation, size, intersection, hasOrientation, axis, orientationAngle;

- (id)init
{
    self = [super init];
    if (self)
    {
        intersection = NO;
        [self inittializeVerts];
        [self createVBO];
        hasOrientation = false;
    }
    
    return self;
}

#pragma mark -- Mesh resizing functions

- (void)inittializeVerts
{
    tempVertices.push_back(glm::vec3(0.f, 1.f, 0.f));
    tempVertices.push_back(glm::vec3(0.f, 1.f, 1.f));
    tempVertices.push_back(glm::vec3(1.f, 1.f, 0.f));
    tempVertices.push_back(glm::vec3(1.f, 1.f, 1.f));
    tempVertices.push_back(glm::vec3(0.f, 0.f, 0.f));
    tempVertices.push_back(glm::vec3(0.f, 0.f, 1.f));
    tempVertices.push_back(glm::vec3(1.f, 0.f, 0.f));
    tempVertices.push_back(glm::vec3(1.f, 0.f, 1.f));
    
    [self resizeMesh:glm::vec3(1.f)];
    [self centerMeshByExtentsWithCenter:glm::vec3(0.f)];
    
    verts.push_back(tempVertices[PointE]);
    verts.push_back(tempVertices[PointF]);
    
    verts.push_back(tempVertices[PointE]);
    verts.push_back(tempVertices[PointG]);
    
    verts.push_back(tempVertices[PointG]);
    verts.push_back(tempVertices[PointH]);
    
    verts.push_back(tempVertices[PointF]);
    verts.push_back(tempVertices[PointH]);
    
    verts.push_back(tempVertices[PointA]);
    verts.push_back(tempVertices[PointB]);
    
    verts.push_back(tempVertices[PointA]);
    verts.push_back(tempVertices[PointC]);
    
    verts.push_back(tempVertices[PointC]);
    verts.push_back(tempVertices[PointD]);
    
    verts.push_back(tempVertices[PointB]);
    verts.push_back(tempVertices[PointD]);
    
    verts.push_back(tempVertices[PointA]);
    verts.push_back(tempVertices[PointE]);
    
    verts.push_back(tempVertices[PointC]);
    verts.push_back(tempVertices[PointG]);
    
    verts.push_back(tempVertices[PointB]);
    verts.push_back(tempVertices[PointF]);
    
    verts.push_back(tempVertices[PointD]);
    verts.push_back(tempVertices[PointH]);
    
    
    
}

#pragma mark -- Mesh resizing functions

- (void)centerMeshByExtentsWithCenter:(glm::vec3)centerLocation
{
    if (tempVertices.size() < 2)
        return;
    
    glm::vec3 min, max;
    {
        std::vector<glm::vec3>::const_iterator it = tempVertices.begin();
        min = max = *it;
        for (; it != tempVertices.end(); ++it)
        {
            if (min.x > it->x)
                min.x = it->x;
            if (min.y > it->y)
                min.y = it->y;
            if (min.z > it->z)
                min.z = it->z;
            
            if (max.x < it->x)
                max.x = it->x;
            if (max.y < it->y)
                max.y = it->y;
            if (max.z < it->z)
                max.z = it->z;
        }
    }
    glm::vec3 center = (max + min) / 2.f;
    
    glm::vec3 vertexOffset = centerLocation - center;
    for (int i = 0; i < tempVertices.size(); i++)
        tempVertices[i] += vertexOffset;
}

- (void)resizeMesh:(glm::vec3)scale
{
    if (tempVertices.size() < 2)
        return;
    
    glm::vec3 min, max;
    {
        std::vector<glm::vec3>::const_iterator it = tempVertices.begin();
        min = max = *it;
        for (; it != tempVertices.end(); ++it)
        {
            if (min.x > it->x)
                min.x = it->x;
            if (min.y > it->y)
                min.y = it->y;
            if (min.z > it->z)
                min.z = it->z;
            
            if (max.x < it->x)
                max.x = it->x;
            if (max.y < it->y)
                max.y = it->y;
            if (max.z < it->z)
                max.z = it->z;
        }
    }
    
    glm::vec3 extent = max - min;
    glm::vec3 resize = scale / std::max(extent.x,
                                        std::max(extent.y,
                                                 extent.z));
    
    for (int i = 0; i < tempVertices.size(); i++)
        tempVertices[i] *= resize;
}

- (void)createVBO
{    
    shader = [[ShaderManager sharedManager] createShader:@"BoundingBox" withFile:@"BoundingBox.plist"];
    
    //create VAO
    glGenVertexArraysOES(1, &vao_handle);
    glBindVertexArrayOES(vao_handle);
    
    glGenBuffers(1, &verticesVBO);
    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
    glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(glm::vec3),
                 &verts.front(), GL_STATIC_DRAW);
    [shader bindAttribute:@"aPosition" withSize:3];
    
    glBindVertexArrayOES(0);
}

- (void)render
{
    // set line width of bbox
    glLineWidth(3.f);
    
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    
    if (hasOrientation)
    {
        modelMatrix = glm::rotate(modelMatrix, -orientationAngle, glm::vec3(0.f, axis.y, 0.f));
    }
    else
    {
        modelMatrix = glm::rotate(modelMatrix, 0.f, glm::vec3(0.f, 1.f, 0.f));
        modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
        modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
        modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    }
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    [[ShaderManager sharedManager] useShader:@"BoundingBox"];
    
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    
    if (intersection)
    {
        color = glm::vec4(1.f, 0.f, 0.f, 1.f);
        intersection = NO;
    }
    else
    {
        color = glm::vec4(1.f, 1.f, 1.f, 1.f);
    }
    glUniform4fv([shader uniformLocation:@"uColor"], 1, glm::value_ptr(color));
    
    glBindVertexArrayOES(vao_handle);
    glDrawArrays(GL_LINES, 0, NUM_VERTICES);
    [[ShaderManager sharedManager] disableCurrentShader];
    glBindVertexArrayOES(0);
}

- (std::vector<glm::vec3>)getPoints
{
    
    glm::vec3 p1 = glm::vec3(position.x - (size.x / 2), position.y - (size.y / 2), position.z - (size.z / 2));
    glm::vec3 p2 = glm::vec3(position.x + (size.x / 2), position.y - (size.y / 2), position.z - (size.z / 2));
    glm::vec3 p3 = glm::vec3(position.x - (size.x / 2), position.y + (size.y / 2), position.z - (size.z / 2));
    glm::vec3 p4 = glm::vec3(position.x + (size.x / 2), position.y + (size.y / 2), position.z - (size.z / 2));
    glm::vec3 p5 = glm::vec3(position.x - (size.x / 2), position.y - (size.y / 2), position.z + (size.z / 2));
    glm::vec3 p6 = glm::vec3(position.x + (size.x / 2), position.y - (size.y / 2), position.z + (size.z / 2));
    glm::vec3 p7 = glm::vec3(position.x - (size.x / 2), position.y + (size.y / 2), position.z + (size.z / 2));
    glm::vec3 p8 = glm::vec3(position.x + (size.x / 2), position.y + (size.y / 2), position.z + (size.z / 2));
    
    std::vector<glm::vec3> points;
    
    points.push_back(p1);
    points.push_back(p2);
    points.push_back(p3);
    points.push_back(p4);
    points.push_back(p5);
    points.push_back(p6);
    points.push_back(p7);
    points.push_back(p8);
    
    return points;
}

- (BOOL)collisionTest:(BoundingBox *)bbox
{
    std::vector<glm::vec3> points = [self getPoints];
    //printf("Player: %f %f %f  Object: %f %f %f\n", position.x, position.y, position.z, bbox.position.x, bbox.position.y, bbox.position.z);
    for (int i = 0; i < points.size(); i++) {
        glm::vec3 p = points[i];
        
        if (p.x < bbox.position.x + (bbox.size.x / 2) && p.x > bbox.position.x - (bbox.size.x / 2) &&
            p.y < bbox.position.y + (bbox.size.y / 2) && p.y > bbox.position.y - (bbox.size.y / 2) &&
            p.z < bbox.position.z + (bbox.size.z / 2) && p.z > bbox.position.z - (bbox.size.z / 2)
            )
        {
            //printf("Collision!\n");
            intersection = YES;
            return YES;
        }
    }
    return NO;
}


@end
