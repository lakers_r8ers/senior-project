//
//  BoundingBox.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 3/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "glm.hpp"
#import "type_ptr.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "CameraManager.h"
#import <vector>

@interface BoundingBox : NSObject
{
    // model matrix attributes
    glm::mat4 modelMatrix;
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 size;
    
    ShaderUtil *shader;
    GLuint verticesVBO;
    GLuint vao_handle;
    
    glm::vec4 color;
    
    // change color when intersection occurs
    BOOL intersection;
    
    BOOL hasOrientation;
    glm::vec3 axis;
    float orientationAngle;
    
    // vertices variables
    std::vector<glm::vec3> tempVertices;
    std::vector<glm::vec3> verts;
    
}

@property (nonatomic) glm::vec3 position;
@property (nonatomic) glm::vec3 rotation;
@property (nonatomic) glm::vec3 size;
@property (nonatomic) BOOL intersection;
@property (nonatomic) BOOL hasOrientation;
@property (nonatomic) glm::vec3 axis;
@property (nonatomic) float orientationAngle;

- (void)render;
- (BOOL)collisionTest:(BoundingBox *)bbox;
- (std::vector<glm::vec3>)getPoints;

@end
