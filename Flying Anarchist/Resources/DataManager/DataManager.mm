//
//  DataManager.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/27/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

static DataManager *sharedDataManager;

//Directory where zones are saved to
NSString *savePath = @"FlyingAnarchist";

#pragma mark - Singleton

+ (DataManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedDataManager = [[DataManager alloc] init];
    }
    return sharedDataManager;
}

#pragma mark - Initialization

- (id)init
{
    self  = [super init];
    if (self)
    {
        [self loadZone];
    }
    return self;
}

#pragma mark - Saving Data

- (void)saveZone:(ObstacleZone *)zone
{
    BOOL isDir;
    [[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath:savePath] isDirectory:&isDir];
    NSLog(@"%d", isDir);
    if (isDir != YES)
    {
        NSLog(@"Directory does not exist");
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:[self dataFilePath:savePath]
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create directory error: %@", error);
        }
        
    }
    else
    {
        NSLog(@"Directory exists");
    }
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:zone forKey:zone.name];
    [archiver finishEncoding];
    
    NSString *path = [self dataFilePath:[savePath stringByAppendingFormat:@"/%@.data", zone.name]];
    [data writeToFile:path atomically:YES];
    [self writeZoneToPlist:zone.name];
}

- (void)writeZoneToPlist:(NSString *)name
{
    NSString *path = [self dataFilePath:[savePath stringByAppendingFormat:@"/Data.plist"]];
    [zoneListNames setValue:name forKey:name];
    [zoneListNames writeToFile:path atomically:YES];
}

#pragma mark - Loading Data
- (void)loadZone
{
    zoneListNames = [self loadZoneNamesFromPlist];
    if ([zoneListNames count])
    {
        for (NSString *zoneName in zoneListNames)
        {
            NSString *path = [self dataFilePath:[savePath stringByAppendingFormat:@"/%@.data", zoneName]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:path])
            {
                    [EAGLContext setCurrentContext:[RenderManager sharedManager].context];
                    NSData *data = [[NSMutableData alloc] initWithContentsOfFile:path];
                    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
                    ObstacleZone *zone = [unarchiver decodeObjectForKey:zoneName];
                    [unarchiver finishDecoding];
                    [[ZoneListManager sharedManager] addZone:zone withName:zone.name];
            }
        }
    }
}

- (NSMutableDictionary *)loadZoneNamesFromPlist
{
    NSString *path = [self dataFilePath:[savePath stringByAppendingFormat:@"/Data.plist"]];
    NSMutableDictionary *dict;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSLog(@"File exists");
        dict = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    }
    else
    {
        return [[NSMutableDictionary alloc] init];
    }
    return dict;
}

#pragma mark - Retrieve Data Path
- (NSString *)dataFilePath:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:name];
}

#pragma mark - Erasing Data
- (void)eraseDataForZone:(NSString *)zoneName
{
    //locate path to zone data
    NSString *path = [self dataFilePath:[savePath stringByAppendingFormat:@"/Data.plist"]];
    NSString *dataPath = [self dataFilePath:[savePath stringByAppendingFormat:@"/%@.data", zoneName]];
    
    //delete file
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:dataPath error:&error];
    if (!success)
    {
        NSLog(@"Error removing document path: %@", error.localizedDescription);
    }
    
    //Delete zone from plist file
    [zoneListNames removeObjectForKey:zoneName];
    [zoneListNames writeToFile:path atomically:YES];
}

@end
