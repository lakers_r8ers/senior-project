//
//  DataManager.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/27/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObstacleZone.h"
#import "ZoneListManager.h"
#import "RenderManager.h"

@interface DataManager : NSObject
{
    NSMutableDictionary *zoneListNames;
}
+ (DataManager *)sharedManager;

- (void)saveZone:(ObstacleZone *)zone;
- (void)eraseDataForZone:(NSString *)zoneName;
@end
