//
//  EditorManager.h
//  Flying Anarchist
//
//  Created by Lion User on 09/01/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"
#import "Building.h"
#import "Skybox.h"
#import "ObstacleZone.h"
#include "MissleSpawn.h"
#include "Tunnel.h"

@class GameObject;
@class ObstacleZone;

@interface EditorManager : NSObject
{
   // NSMutableArray *objectsArray;
    GameObject *currentObject;
    ObstacleZone *obstacleZone;
    Skybox *skybox;
}

@property (nonatomic) GameObject *currentObject;
@property (nonatomic) ObstacleZone *obstacleZone;

+ (EditorManager *)sharedManager;

- (void)update;
- (void)render;

- (void)addBuilding:(int)buildingType;
- (void)addMissleSpawn;
- (void)addTunnel;
- (void)changeGroundToType:(int)type;
- (void)deleteCurrentObject;
- (void)initWithNewZone;
- (void)loadZone:(ObstacleZone *)oldZone;

@end
