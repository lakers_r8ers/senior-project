//
//  EditorManager.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 09/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "EditorManager.h"

@implementation EditorManager

@synthesize currentObject, obstacleZone;

static EditorManager *sharedEditorManager;

#pragma mark - Singleton

+ (EditorManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedEditorManager = [[EditorManager alloc] init];
    }
    return sharedEditorManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        skybox = [[Skybox alloc] init];
    }
    return self;
}

- (void)initWithNewZone
{
    obstacleZone = [[ObstacleZone alloc] init];
}

- (void)loadZone:(ObstacleZone *)oldZone
{
    obstacleZone = oldZone;
}

#pragma mark -- Object Creation

- (void)addBuilding:(int)buildingType
{
    currentObject = [[Building alloc] initWithBuildingType:buildingType];
    [obstacleZone addGameObject:currentObject];
}

- (void)addMissleSpawn
{
    currentObject = [[MissleSpawn alloc] init];
    [currentObject setPosition:glm::vec3(0.f, 5.f, 0.f)];
    [currentObject setSize:glm::vec3(10.f)];
    [obstacleZone addGameObject:currentObject];
}

- (void)addTunnel
{
    currentObject = [[Tunnel alloc] init];
    [currentObject setPosition:glm::vec3(0.f, 2.f, 0.f)];
    [currentObject setSize:glm::vec3(glm::vec3(20.f, 30.f, 50.f))];
    [obstacleZone addGameObject:currentObject];
}

- (void)changeGroundToType:(int)type
{
    [obstacleZone changeGroundToType:type];
}

- (void)deleteCurrentObject
{
    [obstacleZone deleteGameObject:currentObject];
    currentObject = nil;
}

- (void)update
{
    
}

- (void)render
{
    [skybox render];
    [obstacleZone render];
    
    //if (currentObject != nil)
        //[currentObject renderBBox];
}

@end
