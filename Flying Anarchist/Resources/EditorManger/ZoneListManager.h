//
//  ZoneListManager.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObstacleZone.h"
#import "DataManager.h"

@class ObstacleZone;

@interface ZoneListManager : NSObject
{
    NSMutableDictionary *zoneList;
}

@property (nonatomic) NSMutableDictionary *zoneList;

+ (ZoneListManager *)sharedManager;

- (void)addZone:(ObstacleZone *)zone withName:(NSString *)name;
- (ObstacleZone *)getZone:(NSString *)name;
- (void)removeItemWithName:(NSString *)name;

@end
