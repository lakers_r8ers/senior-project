//
//  ZoneListManager.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "ZoneListManager.h"

@implementation ZoneListManager

@synthesize zoneList;

static ZoneListManager *sharedZoneListManager;

#pragma mark - Singleton

+ (ZoneListManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedZoneListManager = [[ZoneListManager alloc] init];
    }
    return sharedZoneListManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        zoneList = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addZone:(ObstacleZone *)zone withName:(NSString *)name
{
    [zoneList setObject:zone forKey:name];
}

- (ObstacleZone *)getZone:(NSString *)name
{
    return [zoneList objectForKey:name];
}

- (void)removeItemWithName:(NSString *)name
{
    [zoneList removeObjectForKey:name];
    [[DataManager sharedManager] eraseDataForZone:name];
}

@end
