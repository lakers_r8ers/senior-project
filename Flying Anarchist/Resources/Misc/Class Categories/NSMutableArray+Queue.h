//
//  NSMutableArray+Queue.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/2/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Queue)

- (void) enqueue: (id)item;
- (id) dequeue;
- (id) peek;

@end
