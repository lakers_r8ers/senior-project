//
//  PickingRay.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 3/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CameraManager.h"
#import "glm.hpp"
#import "GameObject.h"

@class GameObject;

@interface PickingRay : NSObject
{
    // window width and height
    GLsizei width;
    GLsizei height;
    
    // screen point
    CGPoint screenLoc;
    
    // world position near and far
    glm::vec3 worldPosNear;
    glm::vec3 worldPosFar;
    
    // direction of ray
    glm::vec3 direction;
}

- (id)initWithView:(UIView *)view screenPoint:(CGPoint)loc;
- (bool)testZIntersection:(GameObject*)g;
- (bool)testXIntersection:(GameObject *)g;
- (bool)testYIntersection:(GameObject *)g;

@end
