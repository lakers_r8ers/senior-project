//
//  PickingRay.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 3/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "PickingRay.h"

@implementation PickingRay

- (id)initWithView:(UIView *)view screenPoint:(CGPoint)loc
{
    self = [super init];
    if (self)
    {
        width = view.bounds.size.width;
        height = view.bounds.size.height;
        screenLoc = loc;
        [self calculateRay];
    }
    return self;
}

- (void)calculateRay
{
    glm::vec4 viewport = glm::vec4(0.f, 0.f, width, height);
    glm::mat4 viewMatrix = [[CameraManager sharedManager] viewMatrix];
    glm::mat4 projectionMatrix = [[CameraManager sharedManager] projectionMatrix];
    
    // get the near point of the screen click
    glm::vec3 screenPosNear = glm::vec3(screenLoc.x, height - screenLoc.y , 0.f);
    worldPosNear = glm::unProject(screenPosNear, viewMatrix, projectionMatrix, viewport);
    
    // get the far point of the screen click
    glm::vec3 screenPosFar = glm::vec3(screenLoc.x, height - screenLoc.y, 1.f);
    worldPosFar = glm::unProject(screenPosFar, viewMatrix, projectionMatrix, viewport);
    
    // determine the direction of the picking ray
    direction = worldPosFar - worldPosNear;
}

#pragma mark - ray intersection tests
/*
 * Take the near and far point, create a line and see if any game objects intersect
 * with a point on that line 
 */
- (bool)testZIntersection:(GameObject *)g
{
    float z = g.position.z;
    float t = (z - worldPosNear.z) / direction.z;
    float y = worldPosNear.y + (direction.y * t);
    float x = worldPosNear.x + (direction.x * t);
    
    if (x <= (g.position.x + (g.bbox.size.x / 2)) && x >= (g.position.x - (g.bbox.size.x / 2)) &&
        y <= (g.position.y + (g.bbox.size.y / 2)) && y >= (g.position.y - (g.bbox.size.y / 2)))
    {
        return true;
    }
    
    return false;
}

- (bool)testXIntersection:(GameObject *)g
{
    float x = g.position.x;
    float t = (x - worldPosNear.x) / direction.x;
    float y = worldPosNear.y + (direction.y * t);
    float z = worldPosNear.z + (direction.z * t);
    
    if (z <= (g.position.z + (g.bbox.size.z / 2)) && z >= (g.position.z - (g.bbox.size.z / 2)) &&
        y <= (g.position.y + (g.bbox.size.y / 2)) && y >= (g.position.y - (g.bbox.size.y / 2)))
    {
        return true;
    }
    
    return false;
}

- (bool)testYIntersection:(GameObject *)g
{
    float y = g.position.y;
    float t = (y - worldPosNear.y) / direction.y;
    float x = worldPosNear.x + (direction.x * t);
    float z = worldPosNear.z + (direction.z * t);
    
    if (x <= (g.position.x + (g.bbox.size.x / 2)) && x >= (g.position.x - (g.bbox.size.x / 2)) &&
        z <= (g.position.z + (g.bbox.size.z / 2)) && z >= (g.position.z - (g.bbox.size.z / 2)))
    {
        return true;
    }
    
    return false;
}


@end

