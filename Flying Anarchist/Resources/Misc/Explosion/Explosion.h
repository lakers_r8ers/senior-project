//
//  Explosion.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 5/22/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "GameObject.h"
#import "MeshUtil.h"
#import "MeshManager.h"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "CameraManager.h"

@interface Explosion : GameObject
{
    MeshUtil *mesh;
    ShaderUtil *shader;
    glm::mat4 modelMatrix;
    float explosionSize;
    BOOL shouldExplode;
}

- (void)update;


@end
