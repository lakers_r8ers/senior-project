//
//  Explosion.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 5/22/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "Explosion.h"

@implementation Explosion

- (id)init
{
    self = [super init];
    if (self)
    {
        size = glm::vec3(0.f);
        modelMatrix = glm::mat4(1.f);
        shader = [[ShaderManager sharedManager] createShader:@"ExplosionShader" withFile:@"Explosion.plist"];
        mesh = [[MeshManager sharedManager] meshForName:@"explosion.obj"];
        [mesh loadWithShader:shader];
        explosionSize = 0;
        shouldExplode = YES;
    }
    
    return self;
}

- (void)update
{
    explosionSize += (shouldExplode) ? 0.08f : 0.f;
    if (explosionSize > 2.f)
    {
        explosionSize = 0.f;
        shouldExplode = NO;
    }
}

- (void)render
{
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, 90.f, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    
    //scale
    size = glm::vec3(explosionSize);
    modelMatrix = glm::scale(modelMatrix, size);
    
    [[ShaderManager sharedManager] useShader:@"ExplosionShader"];
    
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    glUniform1fv([shader uniformLocation:@"camPos"], 3, glm::value_ptr([[CameraManager sharedManager] position]));

    glUniform3fv([shader uniformLocation:@"color"], 1, glm::value_ptr(glm::vec3(1.f, 0.f, 0.f)));
    
    [mesh render];
}

- (void)renderBBox
{
    
}
@end
