//
//  GyroManager.m
//  Flying Anarchist
//
//  Created by Lion User on 08/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "GyroManager.h"

@implementation GyroManager

@synthesize pitch, yaw, roll;

static GyroManager *sharedGyroManager;

#pragma mark - Singleton

+ (GyroManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedGyroManager = [[GyroManager alloc] init];
    }
    return sharedGyroManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        motionManager = [[CMMotionManager alloc] init];
        motionManager.deviceMotionUpdateInterval = 1.0/60.0;
    }
    
    return self;
}

- (void)startGyroscope
{
    if (motionManager.isDeviceMotionAvailable)
    {
        [motionManager startDeviceMotionUpdates];
    }
}

- (void)stopGyroscope
{
    if (motionManager.isDeviceMotionAvailable)
    {
        [motionManager startDeviceMotionUpdates];
    }
}

- (void)update
{
    
    CMDeviceMotion *deviceMotion = motionManager.deviceMotion;
    CMAttitude *attitude = deviceMotion.attitude;
    
    yaw = radiansToDegrees(attitude.yaw);
    pitch = radiansToDegrees(attitude.pitch);
    roll = radiansToDegrees(attitude.roll);
    
    //NSLog(@"yaw = %f, pitch = %f, roll = %f\n", yaw,
    // pitch, roll);
}

@end
