//
//  GyroManager.h
//  Flying Anarchist
//
//  Created by Lion User on 08/01/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

#define radiansToDegrees(radians) ((radians)*(180.0 / M_PI))

@interface GyroManager : NSObject
{
    CMMotionManager *motionManager;
    
    float yaw;
    float pitch;
    float roll;
}

@property (nonatomic) float yaw;
@property (nonatomic) float pitch;
@property (nonatomic) float roll;

+ (GyroManager *)sharedManager;

- (void)startGyroscope;
- (void)stopGyroscope;
- (void)update;

@end
