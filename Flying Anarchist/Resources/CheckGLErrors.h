//
//  CheckGLErrors.h
//  Level Editor
//
//  Created by Lion User on 26/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface CheckGLErrors : NSObject

+ (void)printErrors:(NSString *)string;

@end