//
//  VFCulling.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/25/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CameraManager.h"
#import "BoundingBox.h"
#import <vector>

enum FRUSTRUM_ENUMS
{
    TOP, BOTTOM, LEFT, RIGHT, NEARP, FARP
};

enum OBJECT_LOCATION
{
    INSIDE, OUTSIDE, INTERSECT
};

struct Plane
{
    glm::vec3 normal;
    glm::vec3 point;
	float distance;
};

@interface VFCulling : NSObject
{
    // view frustrum setup vars
    glm::vec3 position; // camera position
    glm::vec3 look; // direction vector of the camera's ray. should be normalized
    glm::vec3 up;
    glm::vec3 right;
    
    float nearD; // near plane
    float nh; // the height of the near plane
    float nw; // the width of the near plane
    float farD; // the distance from the camera to the far plane
    float fh; // the height of the far plane
    float fw; // the width of the far plane
    
    // points that define the far plane
    glm::vec3 fc; // center
    glm::vec3 ftl; // top left;
    glm::vec3 ftr; // top right
    glm::vec3 fbl; // bottom left;
    glm::vec3 fbr; // bottom right;
    
    // points that define the near plane
    glm::vec3 nc; // center
    glm::vec3 ntl; // top left;
    glm::vec3 ntr; // top right
    glm::vec3 nbl; // bottom left;
    glm::vec3 nbr; // bottom right;
    
    Plane plane[6];
    
    int rendered;
    int notRendered;
}

+ (VFCulling *)sharedManager;
- (void)setCamInternals;
- (void)setCamDef;
- (int)boxInFrustrum:(BoundingBox *)bbox;
- (void)printRendered;

@end
