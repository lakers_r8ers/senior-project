//
//  VFCulling.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/25/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "VFCulling.h"

#define ANG2RAD 3.14159265358979323846/180.0

@implementation VFCulling
static VFCulling *sharedVFCullingManager;

#pragma mark - Singleton

+ (VFCulling *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedVFCullingManager = [[VFCulling alloc] init];
    }
    return sharedVFCullingManager;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    
    return self;
}

- (void)setCamInternals
{
    // camera
    up = [[CameraManager sharedManager] upDirection];
    position = [[CameraManager sharedManager] position];
    look = [[CameraManager sharedManager] center];
    
    // plane distances
    nearD = [[CameraManager sharedManager] nearClip] * 0.5;
    farD = [[CameraManager sharedManager] farClip];
    
    // width and height of the near and far plane sections
    float tang = (float)tan(ANG2RAD * [[CameraManager sharedManager] fov]);
    float ratio = [[CameraManager sharedManager] aspect];
    nh = nearD * tang;
    nw = nh * ratio;
    fh = farD * tang;
    fw = fh * ratio;
}

- (void)setCamDef
{
    glm::vec3 dir;
    glm::vec3 X, Y, Z;
    
    // compute z axis camera
    Z = glm::normalize(glm::vec3(position - look));
    
    // compute x axis of camera
    X = glm::normalize(glm::cross(up, Z));
    
    // compute Y axis
    Y = glm::cross(Z, X);
    
    // compute the centers of the near and far planes
    nc = position - Z * nearD;
    fc = position - Z * farD;
    
    // compute the 4 corners of the frustrum on the near plane
    ntl = nc + Y * nh - X * nw;
    ntr = nc + Y * nh + X * nw;
    nbl = nc - Y * nh - X * nw;
	nbr = nc - Y * nh + X * nw;
    
    // compute the 4 corners of the frustum on the far plane
	ftl = fc + Y * fh - X * fw;
	ftr = fc + Y * fh + X * fw;
	fbl = fc - Y * fh - X * fw;
	fbr = fc - Y * fh + X * fw;
    
    plane[TOP] = set_points(ntr, ntl, ftl);
	plane[BOTTOM] = set_points(nbl, nbr, fbr);
	plane[LEFT] = set_points(ntl, nbl, fbl);
	plane[RIGHT] = set_points(nbr, ntr, fbr);
	plane[NEARP] = set_points(ntl, ntr, nbr);
	plane[FARP] = set_points(ftr, ftl, fbl);
}

Plane set_points(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3)
{
    Plane p;
    glm::vec3 aux1;
    glm::vec3 aux2;
    
    aux1 = v1 - v2;
    aux2 = v3 - v2;
    
    p.normal = glm::normalize(glm::cross(aux2, aux1));
    p.point = v2;
    p.distance = -glm::dot(p.normal, p.point);
    
    
    return p;
}

- (int)boxInFrustrum:(BoundingBox *)bbox
{
    int result = INSIDE;
    int outside, inside;
    
    std::vector<glm::vec3> points = [bbox getPoints];
    
    for (int i = 0; i < 6; i++)
    {
        outside = 0;
        inside = 0;
        
        for (int j = 0; j < 8 && (inside == 0 || outside == 0); j++)
        {
            float planeDistance = plane[i].distance + glm::dot(plane[i].normal, points[i]);
            if (planeDistance < 0)
            {
                outside++;
            }
            else
            {
                inside++;
            }
        }
        if (!inside)
        {
            notRendered++;
            return OUTSIDE;
        }
        else if (outside)
            result = INTERSECT;
    }
    rendered++;
    return result;
}

- (void)printRendered
{
    //printf("Rendered: %d Not Rendered: %d\n", rendered, notRendered);
    rendered = 0;
    notRendered = 0;
}



@end
