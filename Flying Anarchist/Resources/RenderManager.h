//
//  RenderManager.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/27/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RenderManager : NSObject
{
    EAGLContext *context;
}

@property (nonatomic) EAGLContext *context;

+ (RenderManager *)sharedManager;
@end
