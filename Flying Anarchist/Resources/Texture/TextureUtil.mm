//
//  TextureUtil.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 16/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "TextureUtil.h"

@implementation TextureUtil

@synthesize pixels;
@synthesize width, height;
@synthesize textureID, fboID;

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithTexture:(NSString *)imageName
            TexHandle:(GLuint)texHandle
{
    textureID = texHandle;
    
    [self loadImage:imageName];
    return [self init];
}

- (void)loadImage:(NSString *)filename
{
    //Bind texture object
    glBindTexture(GL_TEXTURE_2D, textureID);
    
    NSData *texData = [[NSData alloc] initWithContentsOfFile:filename];
    UIImage *image = [[UIImage alloc] initWithData:texData];
    if (image == nil)
    {
        NSLog(@"Error with loading @ image content\n");
    }
    
    width = CGImageGetWidth(image.CGImage);
    height = CGImageGetHeight(image.CGImage);
    pixels = (uint8_t *) malloc(height * width * 4 * sizeof(uint8_t));
    
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * 4, CGImageGetColorSpace(image.CGImage), kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, width, height), image.CGImage);
    CGContextRelease(context);
    
    [self generateMipMap];
    
    free(pixels);
}

- (void)generateMipMap
{
    //Load the texture
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 width,
                 height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    // Set up filter and wrap modes for this texture object
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    // Indicate that pixel rows are tightly packed
	//  (defaults to stride of 4 which is kind of only good for
	//  RGBA or FLOAT data types)
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glGenerateMipmap(GL_TEXTURE_2D);
}

- (void)generateRepeatingTexture
{
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
}

- (void)loadTextureToShader:(ShaderUtil *)shader
                  usingName:(NSString *)name
{
    GLuint handle = [shader uniformLocation:name];
    glBindTexture(GL_TEXTURE_2D, textureID);
    glUniform1i(handle, 0);
}

- (void)dealloc
{
    glDeleteRenderbuffers(1, &depthBufferID);
    glDeleteFramebuffers(1,&fboID);
    glDeleteTextures(1, &textureID);
}
@end
