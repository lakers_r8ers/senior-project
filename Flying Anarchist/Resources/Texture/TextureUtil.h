//
//  TextureUtil.h
//  Flying Anarchist
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShaderUtil.h"
#import "CheckGLErrors.h"

@class ShaderUtil;

@interface TextureUtil : NSObject
{
    //object handles
    GLuint textureID;
    GLuint fboID;
    GLuint depthBufferID;
    
    //Image
    UIImage *cImage;
    
    //TexImage2D data
    int width;
    int  height;
    uint8_t* pixels;
}

@property (nonatomic) uint8_t *pixels;
@property (nonatomic) int width;
@property (nonatomic) int height;
@property (nonatomic) GLuint textureID;
@property (nonatomic) GLuint fboID;

- (id)initWithTexture:(NSString *)imageName
            TexHandle:(GLuint)texHandle;
- (void)loadTextureToShader:(ShaderUtil *)shader
          usingName:(NSString *)name;
- (void)generateMipMap;
- (void)generateRepeatingTexture;


@end
