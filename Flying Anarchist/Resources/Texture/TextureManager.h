//
//  TextureManager.h
//  Flying Anarchist
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextureUtil.h"

@class TextureUtil;

@interface TextureManager : NSObject
{
    NSMutableDictionary *textureList;
    GLuint *texture_handles;
    GLuint *fbo_handles;
    GLuint *depthBuffer_handles;
}

+ (TextureManager *)sharedManager;

- (void)createTextures;
- (TextureUtil *)textureForName:(NSString *)name;

@end
