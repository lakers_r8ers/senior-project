//
//  TextureManager.m
//  Flying Anarchist
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "TextureManager.h"

@implementation TextureManager

static TextureManager *sharedTextureManager;

#pragma mark - Singleton

+ (TextureManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedTextureManager = [[TextureManager alloc] init];
    }
    return sharedTextureManager;
}

#pragma mark - Initialzations

- (id)init
{
    if (self = [super init])
    {
        textureList = [[NSMutableDictionary alloc] init];
        [self createTextures];
    }
    
    return self;
}


#pragma mark - MeshManager openrations

- (void)createTextures
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"TextureList.plist" ofType:@""];
    NSArray *textureFileList = [[NSArray alloc] initWithContentsOfFile:file];
    
    //Initialize handle buffers
    texture_handles = (GLuint *)malloc(sizeof(GLuint) * [textureFileList count]);
    fbo_handles = (GLuint *)malloc(sizeof(GLuint) * [textureFileList count]);
    depthBuffer_handles = (GLuint *)malloc(sizeof(GLuint) * [textureFileList count]);
    
    //generate handle buffers
    glGenTextures([textureFileList count], texture_handles);
    glGenFramebuffers([textureFileList count], fbo_handles);
    glGenRenderbuffers([textureFileList count], depthBuffer_handles);
    
    int i = 0;
    for (NSString *name in textureFileList)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:nil];
        [textureList setObject:[[TextureUtil alloc]
          initWithTexture:path TexHandle:texture_handles[i++]] forKey:name];
    }
}

- (TextureUtil *)textureForName:(NSString *)name
{
    return [textureList objectForKey:name];
}

- (void)dealloc
{
    free(texture_handles);
}

@end
