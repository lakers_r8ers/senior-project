//
//  CameraManager.m
//  Flying Anarchist
//
//  Created by Lion User on 27/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "CameraManager.h"

#define DEG_2_RAD(angle) ((angle) / 180.0 * M_PI)

@implementation CameraManager

@synthesize position;
@synthesize center;
@synthesize upDirection;
@synthesize viewMatrix;
@synthesize projectionMatrix;
@synthesize aspect;
//@synthesize startRotation, curRotation;
@synthesize shouldUpdateRotation;
@synthesize fov, nearClip, farClip;
@synthesize radius;


static CameraManager *sharedCameraManager;

#pragma mark - Singleton

+ (CameraManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedCameraManager = [[CameraManager alloc] init];
    }
    return sharedCameraManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        theta = 0;
        phi = 0;
        
        shouldUpdateRotation = NO;
        
        fov = 65.f;
        nearClip = 0.05f;
        farClip = 250.f;
        projectionMatrix = glm::perspective(fov, aspect, nearClip,
                                            farClip);
        
        position = glm::vec3(0.f, 4.f, -6.f);
        upDirection = glm::vec3(0, 1, 0);
        center = glm::vec3(0.f, 0.f, 0.f);
        viewMatrix = glm::lookAt(position, center, upDirection);
    }
    return self;
}

#pragma mark - uodate method
- (void)update
{
    //update projection matrix
    projectionMatrix = glm::perspective(fov, aspect, nearClip, farClip);
    viewMatrix = glm::lookAt(position, center, upDirection);
    [[VFCulling sharedManager] setCamInternals];
    [[VFCulling sharedManager] setCamDef];
}

#pragma mark - rotation
- (void)setTheta:(float)thetaVal andPhi:(float)phiVal
{
    float oldTheta = theta;
    float oldPhi = phi;
    
    theta += thetaVal;
    phi += phiVal;
    
    position = glm::vec3(center.x + (radius*cos(phi)*cos(theta)),
                         center.y - (radius*sin(phi)),
                         center.z + (radius*cos(phi)*sin(theta)));
    if (position.y < 1.f)
    {
        position = glm::vec3(position.x,
                             1.f,
                             position.z);
        theta = oldTheta;
        phi = oldPhi;
    }
}

#pragma mark - zoom control
- (void)setZoom:(float)radiusVal
{
    radius = radiusVal;
    position = glm::vec3(center.x + (radius*cos(phi)*cos(theta)),
                         center.y - (radius*sin(phi)),
                         center.z + (radius*cos(phi)*sin(theta)));
}

#pragma mark -- Camera options
- (void)resetCamera
{
    center = glm::vec3(0.f, 1.f, 0.f);
    upDirection = glm::vec3(0, 1, 0);
    theta = 0.f;
    phi = 0.f;
    radius = 20.f;
    [self setTheta:DEG_2_RAD(-90.f) andPhi:0.f];
}

- (void)objectCamera
{
    center = [[[EditorManager sharedManager] currentObject] position];
    upDirection = glm::vec3(0.f, 1.f, 0.f);
    theta = 0.f;
    phi = 0.f;
    radius = 20.f;
    [self setTheta:DEG_2_RAD(-90.f) andPhi:0.f];
}

- (void)overheadCamera
{
    center = [[[EditorManager sharedManager] currentObject] position];
    position = glm::vec3(center.x, 30.f, center.z);
    upDirection = glm::vec3(0.f, 0.f, 1.f);
}

- (void)followObject
{
    center = [[[EditorManager sharedManager] currentObject] position];
    [self setTheta:0.f andPhi:0.f];
}

- (void)playerCamera
{
    center = [[[WorldManager sharedManager] player] position];
    upDirection = glm::vec3(0.f, 1.f, 0.f);
    theta = 0.f;
    phi = 0.f;
    radius = 2.0f;
    fov = 40.f;
    [self setTheta:DEG_2_RAD(-90.f) andPhi:0.f];
}

- (void)deadPlayerCamera
{
    center = [[[WorldManager sharedManager] player] position];
    theta = 0.f;
    phi = 0.f;
    radius = 3.5;
    fov = 40.f;
    [self setTheta:DEG_2_RAD(-90.f) andPhi:0.f];
}

@end
