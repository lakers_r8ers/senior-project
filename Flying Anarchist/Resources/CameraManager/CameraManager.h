//
//  CameraManager.h
//  Flying Anarchist
//
//  Created by Lion User on 27/12/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "EditorManager.h"
#import "WorldManager.h"
#import "VFCulling.h"
#include <math.h>

#define ROTATION_Y_MAX 33.0
#define ROTATION_Y_MIN -145.0

@interface CameraManager : NSObject
{
    glm::vec3 position;
    glm::vec3 center;
    glm::vec3 upDirection;
    glm::mat4 viewMatrix;
    
    // projection matrix values
    glm::mat4 projectionMatrix;
    float aspect;
    float fov;
    float nearClip;
    float farClip;
    
    // angles of rotation
    float theta;
    float phi;
    
    // distance from the camera
    float radius;
    
    bool shouldUpdateRotation;
    
    int currentCamera;
}

@property (nonatomic) glm::mat4 projectionMatrix;
@property (nonatomic) float aspect;
@property (nonatomic) glm::mat4 viewMatrix;
@property (nonatomic) glm::vec3 position;
@property (nonatomic) glm::vec3 center;
@property (nonatomic) glm::vec3 upDirection;
@property (nonatomic) bool shouldUpdateRotation;
@property (nonatomic) float fov;
@property (nonatomic) float nearClip;
@property (nonatomic) float farClip;
@property (nonatomic) float radius;

+ (CameraManager *)sharedManager;

- (void)update;
- (void)objectCamera;
- (void)resetCamera;
- (void)followObject;
- (void)overheadCamera;
- (void)playerCamera;
- (void)deadPlayerCamera;
- (void)setTheta:(float)thetaVal andPhi:(float)phiVal;
- (void)setZoom:(float)radiusVal;

@end
