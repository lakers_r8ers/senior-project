//
//  AudioManager.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/30/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "AudioManager.h"

@implementation AudioManager

static AudioManager *sharedAudioManager;

#pragma mark - Singleton

+ (AudioManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedAudioManager = [[AudioManager alloc] init];
    }
    return sharedAudioManager;
}

#pragma mark - Initialization

- (id)init
{
    self  = [super init];
    if (self)
    {
        //[self initSound];
        //[self initSoundFiles];
    }
    return self;
}

- (void)initSoundFiles
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"SoundEffects.plist" ofType:@""];
    NSArray *soundNamesList = [[NSArray alloc] initWithContentsOfFile:file];
    
    for (NSString *name in soundNamesList)
    {
        NSString *soundEffectPath = [[NSBundle mainBundle] pathForResource:name ofType:@""];
        NSURL *soundEffectURL = [NSURL fileURLWithPath:soundEffectPath];
        SystemSoundID soundEffectID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundEffectURL, &soundEffectID);
        //[soundEffectsList setObject:soundEffectID forKey:name];
    }
}

- (void)initSound
{
    printf("Playing music\n");
    NSString *backgroundMusicPath = [[NSBundle mainBundle] pathForResource:@"engine-sound" ofType:@"mp3"];
	NSURL *backgroundMusicURL = [NSURL fileURLWithPath:backgroundMusicPath];
    NSError *error;
    backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    [backgroundMusicPlayer setDelegate:self];
	[backgroundMusicPlayer setNumberOfLoops:-1];	// Negative number means loop forever
    [backgroundMusicPlayer play];
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
    printf("Music was interrupted\n");
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
    printf("Interruption was ended\n");
}

@end
