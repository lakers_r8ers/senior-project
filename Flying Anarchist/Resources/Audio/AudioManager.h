//
//  AudioManager.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/30/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioManager : NSObject
<AVAudioPlayerDelegate>
{
    AVAudioPlayer *backgroundMusicPlayer;
    BOOL backgroundMusicPlaying;
    BOOL backgroundMusicInterrupted;
    NSMutableDictionary *musicList;
    NSMutableDictionary *soundEffectsList;
    
}
+ (AudioManager *)sharedManager;

@end
