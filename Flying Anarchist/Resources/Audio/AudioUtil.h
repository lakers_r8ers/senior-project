//
//  AudioUtil.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 5/16/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioUtil : NSObject
{
 
}

- (id)initWithAudioWithName:(NSString *)name;

@end
