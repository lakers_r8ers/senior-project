//
//  CheckGLErrors.m
//  Level Editor
//
//  Created by Lion User on 26/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "CheckGLErrors.h"

@implementation CheckGLErrors

+ (void)printErrors:(NSString *)string
{
    GLenum error = glGetError();
    
    printf("GLError at %s: ", string.UTF8String);
    switch (error) {
        case GL_NO_ERROR:
            printf("No error found\n");
            break;
        case GL_INVALID_ENUM:
            printf("Invalid enum\n");
            break;
        case GL_INVALID_VALUE:
            printf("Invalid value\n");
            break;
        case GL_INVALID_OPERATION:
            printf("Invalid operation\n");
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            printf("Invalid framebuffer operation\n");
            break;
        case GL_OUT_OF_MEMORY:
            printf("Out of Memory\n");
            break;
        case GL_STACK_OVERFLOW:
            printf("Stack Overflow\n");
            break;
        case GL_STACK_UNDERFLOW:
            printf("Stack Underflow\n");
            break;
        default:
            break;
    }
}

@end
