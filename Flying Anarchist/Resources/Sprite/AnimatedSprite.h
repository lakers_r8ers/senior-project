//
//  SpriteSheet.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/21/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#include "GameObject.h"
#include "glm.hpp"
#include "type_ptr.hpp"
#include "ShaderManager.h"
#include "ShaderUtil.h"
#import "TextureManager.h"
#import "TextureUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#include "CameraManager.h"
#include <vector>


@interface AnimatedSprite : NSObject
{
    ShaderUtil *shader;
    TextureUtil *texture;
    MeshUtil *mesh;
    
    float u;
    float v;
    
    glm::mat4 modelMatrix;
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 direction;
    glm::vec3 size;
}

@property (nonatomic) glm::vec3 position;

- (void)drawSprite;

@end
