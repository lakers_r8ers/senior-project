//
//  SpriteSheet.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/21/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "AnimatedSprite.h"

@implementation AnimatedSprite

@synthesize position;

- (id)init
{
    self = [super init];
    if (self)
    {
        position = glm::vec3(0.0, 5.0, 0.0);
        modelMatrix = glm::mat4(1.f);
        size = glm::vec3(1.f, 0.f, 1.f);
        shader = [[ShaderManager sharedManager] createShader:@"Sprite" withFile:@"Sprite.plist"];
        
        mesh = [[MeshManager sharedManager] meshForName:@"sprite.obj"];
        [mesh loadWithShader:shader];
        
        texture = [[TextureManager sharedManager] textureForName:@"sprite.png"];
        
        u = 0;
        v = 0;
    }
    return self;
}

- (void)update
{
    
}

- (void)drawSprite
{
    // transparent textures
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    modelMatrix = glm::rotate(modelMatrix, 90.f, glm::vec3(1.f, 0.f, 0.f));

    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    [[ShaderManager sharedManager] useShader:@"Sprite"];
    
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    
    int numColumns = 8;
    int numRows = 6;
    int spriteIndex = 26;
    
    u = (float)(spriteIndex % numColumns) / (float)numColumns;
    v = (float)(spriteIndex / numRows) / (float)numRows;
    
    glUniform1fv([shader uniformLocation:@"u"], 1, &u);
    glUniform1fv([shader uniformLocation:@"v"], 1, &v);
    
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
    glDisable(GL_BLEND);
}
@end
