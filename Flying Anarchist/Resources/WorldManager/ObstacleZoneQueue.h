//
//  ObstacleZoneQueue.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/2/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//
#import "NSMutableArray+Queue.h"
#import "ZoneListManager.h"
#import "ObstacleZone.h"
#import "glm.hpp"

#define ZONE_LENGTH 100

@interface ObstacleZoneQueue : NSObject
{
    NSMutableArray *queue;
    int queueSize;
    int zonesCreated;
    float newZonePos;
    int randomIndex;
}

@property (nonatomic) int queueSize;
@property (nonatomic) NSMutableArray *queue;

- (void)updateWithPlayerPos:(glm::vec3)pos;

@end
