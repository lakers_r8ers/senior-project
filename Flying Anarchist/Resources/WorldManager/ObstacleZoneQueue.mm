//
//  ObstacleZoneQueue.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/2/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "ObstacleZoneQueue.h"

@implementation ObstacleZoneQueue

@synthesize queueSize, queue;

- (id)init
{
    self = [super init];
    if (self)
    {
        queueSize = 0;
        zonesCreated = 0;
        queue = [[NSMutableArray alloc] init];
        [self initializeZoneQueue];
    }
    
    return self;
}

- (void)initializeZoneQueue
{
    for (int i = 0; i < 5; i++)
    {
        [queue enqueue:([self getRandomZone])];
        queueSize++;
    }
}

- (void)updateWithPlayerPos:(glm::vec3)pos
{
    ObstacleZone *front = [queue peek];
    
    if (pos.z > (front.ground.bbox.position.z + (front.ground.bbox.size.z / 2)))
    {
        // pop zone
        [queue dequeue];
        
        // add new zone
        ObstacleZone *zone = [self getRandomZone];
        [queue enqueue:zone];
    }
}

- (ObstacleZone *)getRandomZone
{
    ObstacleZone *zone;
    
    // retrieve a random zone
    if (zonesCreated == 0)
    {
        zone = [[ObstacleZone alloc] init];
    }
    else
    {
        [self getRandomIndex];
        NSString *key = [[[[ZoneListManager sharedManager] zoneList] allKeys] objectAtIndex:randomIndex];
        zone = [[[ZoneListManager sharedManager] zoneList] objectForKey:key];
    }
        
    // create a deep copy of that zone using archiving trick
    ObstacleZone *new_zone = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:zone]];
    [new_zone setMode:GAME];
    
    // calculate new zone position and set obstacles relative to that position
    newZonePos = (zonesCreated++ * ZONE_LENGTH);
    [new_zone setPosition:glm::vec3(0.f, 0.f, newZonePos)];
    [new_zone setZOffset:(newZonePos + 50.f)];
    
    return new_zone;
}

- (void)getRandomIndex
{
    int size = [[[ZoneListManager sharedManager] zoneList] count];
    randomIndex = (arc4random() % (size));
}

@end
