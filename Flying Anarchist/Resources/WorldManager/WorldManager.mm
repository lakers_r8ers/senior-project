//
//  WorldManager.m
//  Flying Anarchist
//
//  Created by Lion User on 06/01/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "WorldManager.h"

@implementation WorldManager

@synthesize player;

static WorldManager *sharedWorldManager;

#pragma mark - Singleton

+ (WorldManager *)sharedManager
{
    static BOOL initialized = NO;
    if (!initialized)
    {
        initialized = YES;
        sharedWorldManager = [[WorldManager alloc] init];
    }
    return sharedWorldManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

- (void)startGame
{
    [self initGameObjects];
    [self resetGameObjects];
    [[CameraManager sharedManager] playerCamera];
    [player setHealth:ALIVE];
    [player setPosition:glm::vec3(0.f, 5.f, 0.f)];
    [sprite setPosition:glm::vec3(0.f, 5.f, 0.f)];
    [[GyroManager sharedManager] startGyroscope];
    explosion = [[Explosion alloc] init];
}

- (void)resetGameObjects
{
    NSMutableArray *zones = [zoneQueue queue];
    
    for (ObstacleZone *z in zones)
    {
        [z reset];
    }
}

- (void)initGameObjects;
{
    player = [[Player alloc] init];
    //sprite = [[AnimatedSprite alloc] init];
    skybox = [[Skybox alloc] init];
    zoneQueue = [[ObstacleZoneQueue alloc] init];
}

- (void)update
{
    [self checkForCollisions];
    if ([player health] == ALIVE)
    {
        for (int i = 0; i < [zoneQueue queueSize]; i++)
        {
            ObstacleZone *zone = [[zoneQueue queue] objectAtIndex:i];
            [zone update];
        }
        [player update];
        [self targetMissles];
        [skybox setPosition:[player position]];
        [[CameraManager sharedManager] playerCamera];
        [zoneQueue updateWithPlayerPos:player.position];
        [[VFCulling sharedManager] printRendered];
    }
    else
    {
        [[CameraManager sharedManager] deadPlayerCamera];
        [explosion update];
    }
}

- (void)render
{
    if ([player health] == DEAD)
    {
        [explosion render];
    }
    else
    {
        [player render];
        //[player renderBBox];
    }
    //[sprite drawSprite];
    [skybox render];
    
    for (int i = 0; i < [zoneQueue queueSize]; i++)
    {
        ObstacleZone *zone = [[zoneQueue queue] objectAtIndex:i];
        [zone render];
    }
    
}

#pragma mark - Target Missles

- (void)targetMissles
{
    NSMutableArray *zones = [zoneQueue queue];
    
    for (ObstacleZone *z in zones)
    {
        for (GameObject *g in z.objectsArray)
        {
            if ([g objectType] == MISSLE)
            {
                MissleSpawn *m = (MissleSpawn *)g;
                float playerMissleDistance = m.position.z - player.position.z;
                if (m.hasDirection == NO && playerMissleDistance >= 50.f &&
                 playerMissleDistance <= 100.f)
                {
                    [self calculatePlayerMissleIntersection:m];
                    [m orientMissleWithPlayerPos:player.position];
                }
            }
        }
    }
}

- (void)calculatePlayerMissleIntersection:(MissleSpawn*)missle
{
    
    // collision point
    glm::vec3 collisionPoint = glm::vec3(player.position.x,
                                         player.position.y,
                                         player.position.z + 50.f);
    
    // missle direction
    missle.direction = glm::normalize(collisionPoint - missle.position);
    
    // calculate t
    float t = (collisionPoint.z - player.position.z) / player.speed;
    
    // missle speed
    float magnitude = sqrt(pow(missle.direction.x, 2) + pow(missle.direction.y, 2) + pow(missle.direction.z, 2));
    float missleSpeed = magnitude / t;
    [missle setSpeed:1.f];
    
    //printf("\n\n-----------Rocket calculation-------------\n");
    //printf("player\n");
    //printf("\tposition: %f %f %f\n", player.position.x, player.position.y, player.position.z);
    //printf("missle\n");
    //printf("\tposition: %f %f %f\n", missle.position.x, missle.position.y, missle.position.z);
    //printf("\tdirection: %f %f %f\n", missle.direction.x, missle.direction.y, missle.direction.z);
    //printf("\tspeed: %f\n", missleSpeed);
    //printf("Collision\n");
    //printf("\tpoint: %f %f %f\n", collisionPoint.x, collisionPoint.y, collisionPoint.z);
    //printf("-----------------------------------------------\n");
    [missle setHasDirection:YES];
}

#pragma mark - Collision Detection

- (void)checkForCollisions
{
    NSMutableArray *gameObjects = [[[zoneQueue queue] peek] objectsArray];
    
    for (GameObject *g in gameObjects)
    {
        switch ([g bboxType])
        {
            case SINGLE_BBOX:
            {
                if ([[player bbox] collisionTest:g.bbox] == YES)
                {
                    [player setHealth:DEAD];
                    [explosion setPosition:player.position];
                    return;
                }
                break;
            }
            case BBOX_LIST:
            {
                NSMutableArray *bboxes = [g bboxList];
                for (BoundingBox *b in bboxes)
                {
                    if ([[player bbox] collisionTest:b] == YES)
                    {
                        [player setHealth:DEAD];
                        [explosion setPosition:player.position];
                        return;
                    }
                }
                break;
            }
        }
    }
}

@end
