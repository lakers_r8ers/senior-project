//
//  WorldManager.h
//  Flying Anarchist
//
//  Created by Lion User on 06/01/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
#import "Skybox.h"
#import "ObstacleZoneQueue.h"
#import "GyroManager.h"
#import "AnimatedSprite.h"
#import "Explosion.h"


@class Player;
@class ObstacleZoneQueue;
@class AnimatedSprite;
@class Explosion;

@interface WorldManager : NSObject
{
    Player *player;
    AnimatedSprite *sprite;
    Skybox *skybox;
    ObstacleZoneQueue *zoneQueue;
    Explosion *explosion;
}

@property (nonatomic) Player *player;

+ (WorldManager *)sharedManager;

- (void)startGame;
- (void)update;
- (void)render;

@end
