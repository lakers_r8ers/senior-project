//
//  AppDelegate.h
//  Flying Anarchist
//
//  Created by Lion User on 26/12/2012.
//  Copyright (c) 2012 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraManager.h"
#import "EditorManager.h"
#import "MeshManager.h"
#import "ShaderManager.h"
#import "TextureManager.h"

@class ViewController;
@class LevelEditorViewController;
@class ZoneListViewController;
@class MainScreenViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) LevelEditorViewController *levelEditorViewController;
@property (strong, nonatomic) ZoneListViewController *zoneListViewController;
@property (strong, nonatomic) MainScreenViewController *mainScreenViewController;

@end
