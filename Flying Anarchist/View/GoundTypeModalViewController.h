//
//  GoundTypeModalViewController.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 5/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditorManager.h"

@interface GoundTypeModalViewController : UIViewController
{
    IBOutlet UIButton *ground1Button;
    IBOutlet UIButton *ground2Button;
    IBOutlet UIButton *ground3Button;
    IBOutlet UIButton *ground4Button;
    
    UIButton *currentButton;
    int currentGroundType;
}

- (IBAction)ground1ButtonPressed:(id)sender;
- (IBAction)ground2ButtonPressed:(id)sender;
- (IBAction)ground3ButtonPressed:(id)sender;
- (IBAction)ground4ButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

@end
