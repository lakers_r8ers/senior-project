//
//  GameViewController.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 3/24/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <GLKit/GLKit.h>
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "CheckGLErrors.h"
#import "CameraManager.h"
#import "EditorManager.h"
#import "TextureManager.h"
#import "RenderManager.h"
#import "PickingRay.h"
#import "ZoneListManager.h"
#import "WorldManager.h"

@interface GameViewController : GLKViewController
{
    IBOutlet UILabel *distanceLabel;
    CGPoint touchStartPos;
}

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

- (void)setupGL;
- (void)tearDownGL;

- (IBAction)pauseButtonPressed:(id)sender;

@end
