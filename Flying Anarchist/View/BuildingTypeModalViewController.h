//
//  BuildingTypeModalViewController.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditorManager.h"

@interface BuildingTypeModalViewController : UIViewController
{
    IBOutlet UIButton *skyscraper1Button;
    IBOutlet UIButton *skyscraper2Button;
    IBOutlet UIButton *skyscraper3Button;
    IBOutlet UIButton *skyscraper4Button;
    
    UIButton *currentButton;
    
    int currentBuildingType;
}

- (IBAction)skyscraper1ButtonPressed:(id)sender;
- (IBAction)skyscraper2ButtonPressed:(id)sender;
- (IBAction)skyScraper3ButtonPressed:(id)sender;
- (IBAction)skyscraper4ButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

@end
