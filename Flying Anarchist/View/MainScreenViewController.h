//
//  MainScreenViewController.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZoneListViewController.h"
#import "GameViewController.h"
#import "RenderManager.h"
#import "CameraManager.h"
#import "TextureManager.h"
#import "ShaderManager.h"
#import "MeshManager.h"
#import "WorldManager.h"
#import "GyroManager.h"
#import "VFCulling.h"
#import "AudioManager.h"

@class ZoneListViewController;

@interface MainScreenViewController : UIViewController
{
}

@property (strong, nonatomic) ZoneListViewController *zoneListViewController;
@property (strong, nonatomic) GameViewController *gameViewController;

- (IBAction)editZonesButtonPressed:(id)sender;
- (IBAction)playGameButtonPressed:(id)sender;

@end
