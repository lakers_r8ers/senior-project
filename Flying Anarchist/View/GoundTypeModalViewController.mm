//
//  GoundTypeModalViewController.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 5/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "GoundTypeModalViewController.h"

@implementation GoundTypeModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentGroundType = -1;
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

- (void)doneButtonPressed:(id)sender
{
    [[EditorManager sharedManager] changeGroundToType:currentGroundType];
    [[CameraManager sharedManager] objectCamera];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)cancelButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)ground1ButtonPressed:(id)sender
{
    currentGroundType = GROUND1;
    [currentButton setHighlighted:NO];
    currentButton = ground1Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)ground2ButtonPressed:(id)sender
{
    currentGroundType = GROUND2;
    [currentButton setHighlighted:NO];
    currentButton = ground2Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)ground3ButtonPressed:(id)sender
{
    currentGroundType = GROUND3;
    [currentButton setHighlighted:NO];
    currentButton = ground3Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)ground4ButtonPressed:(id)sender
{
    currentGroundType = GROUND4;
    [currentButton setHighlighted:NO];
    currentButton = ground4Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
