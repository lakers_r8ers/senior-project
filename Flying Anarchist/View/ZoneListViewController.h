//
//  ZoneListViewController.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LevelEditorViewController.h"
#import "ObstacleZone.h"
#import "ZoneListManager.h"
#import "DataManager.h"

@class LevelEditorViewController;

@interface ZoneListViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIBarButtonItem *editButton;
}

@property (strong, nonatomic) LevelEditorViewController *levelEditorViewController;
@property (strong, nonatomic) IBOutlet UITableView *zoneListTableView;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)newZoneButtonPressed:(id)sender;
- (IBAction)editButtonPressed:(id)sender;

@end
