//
//  ZoneListViewController.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "ZoneListViewController.h"

@implementation ZoneListViewController

@synthesize levelEditorViewController, zoneListTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [zoneListTableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Available Zones";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[ZoneListManager sharedManager] zoneList] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    }
    // Retrieve zone from zone list manager
    NSString *key = [[[[ZoneListManager sharedManager] zoneList] allKeys] objectAtIndex:indexPath.row];
    ObstacleZone *zone = [[[ZoneListManager sharedManager] zoneList] objectForKey:key];
    
    [[cell textLabel] setText:[NSString stringWithFormat:(@"%@"), zone.name]];

    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *key = [[[[ZoneListManager sharedManager] zoneList] allKeys] objectAtIndex:indexPath.row];
        [[ZoneListManager sharedManager] removeItemWithName:key];
        [zoneListTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.levelEditorViewController == nil)
    {
        self.levelEditorViewController = [[LevelEditorViewController alloc] initWithNibName:@"LevelEditorViewController" bundle:nil];
    }
    [self presentModalViewController:self.levelEditorViewController animated:YES];
    
    NSString *key = [[[[ZoneListManager sharedManager] zoneList] allKeys] objectAtIndex:indexPath.row];
    ObstacleZone *zone = [[[ZoneListManager sharedManager] zoneList] objectForKey:key];
    [self.levelEditorViewController loadZone:zone];
}

#pragma mark - Button action methods

- (void)backButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)newZoneButtonPressed:(id)sender
{
    if (self.levelEditorViewController == nil)
    {
        self.levelEditorViewController = [[LevelEditorViewController alloc] initWithNibName:@"LevelEditorViewController" bundle:nil];
    }
    [self presentModalViewController:self.levelEditorViewController animated:YES];
    [self.levelEditorViewController createNewZone];
}

- (void)editButtonPressed:(UIBarButtonItem *)sender
{
    if ([zoneListTableView isEditing])
    {
        sender.title = @"Edit";
        [zoneListTableView setEditing:NO animated:YES];
    }
    else
    {
        sender.title = @"Done";
        [zoneListTableView setEditing:YES animated:YES];
    }
}

@end
