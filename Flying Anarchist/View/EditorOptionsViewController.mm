//
//  EditorOptionsViewController.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "EditorOptionsViewController.h"

@implementation EditorOptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [errorMessage setText:@""];
        [nameTextField setText:[[[EditorManager sharedManager] obstacleZone] name]];
    }
    return self;
}

#pragma mark - Orientation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [nameTextField setText:@""];
    [errorMessage setText:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button actions

- (void)backButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)saveButtonPressed:(id)sender
{
    if ([nameTextField.text length] == 0)
    {
        [errorMessage setText:@"Please enter a name for a zone"];
    }
    else
    {
        ObstacleZone *zone = [[EditorManager sharedManager] obstacleZone];
        [zone setName:nameTextField.text];
        [[ZoneListManager sharedManager] addZone:zone withName:nameTextField.text];
        [[DataManager sharedManager] saveZone:zone];
        [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
    }
}

- (void)quitButtonPressed:(id)sender
{
    [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
}

- (void)backgroundTouched:(id)sender
{
    [nameTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
