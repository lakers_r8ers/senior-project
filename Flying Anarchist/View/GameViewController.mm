//
//  GameViewController.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 3/24/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "GameViewController.h"

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[RenderManager sharedManager] context];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [self setupGL];
    //[self initializeGestures];
}

- (void)initializeGestures
{
    UISwipeGestureRecognizer *swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpGesture:)];
    swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUpGesture];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
	self.context = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    glDisable(GL_DEPTH_TEST);
}

#pragma mark - Button action events

-(void)pauseButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - GLKView delegate methods

- (void)update
{
    [[WorldManager sharedManager] update];
    
    //update Camera
    CGFloat scale = UIScreen.mainScreen.scale;
    GLsizei w = self.view.bounds.size.width * scale;
    GLsizei h = self.view.bounds.size.height * scale;
    [CameraManager sharedManager].aspect = fabsf(w / h);
    [[CameraManager sharedManager] update];
    
    // distance text
    float playerDistance = [[WorldManager sharedManager] player].position.z;
    NSString *distanceText = [NSString stringWithFormat:@"%d", (int)playerDistance];
    [distanceLabel setText:distanceText];
    
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    CGFloat scale = UIScreen.mainScreen.scale;
    GLsizei w = self.view.bounds.size.width * scale;
    GLsizei h = self.view.bounds.size.height * scale;
    glViewport(0, 0, w, h);
    glClearColor(0.1f, 0.1f, 0.7f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    [[WorldManager sharedManager] render];
}

#pragma mark - gestures

- (IBAction)swipeUpGesture:(id)sender
{
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count < 2)
    {
        for (UITouch *touch in touches)
        {
            CGPoint loc = [touch locationInView:self.view];
            touchStartPos = loc;
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count < 2)
    {
        for (UITouch *touch in touches)
        {
            float sensitivity = 0.01f;
            CGPoint loc = [touch locationInView:self.view];
            float heightChange = (loc.y - touchStartPos.y) * sensitivity;
            [[[WorldManager sharedManager] player] setPlayerHeight:-heightChange];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        [[[WorldManager sharedManager] player] setAnimationState:STABLE];
    }
}


@end
