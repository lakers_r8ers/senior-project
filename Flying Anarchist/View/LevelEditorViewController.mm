//
//  LevelEditorViewController.m
//  Flying Anarchist
//
//  Created by Lion User on 12/20/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "LevelEditorViewController.h"

#define DEG_2_RAD(angle) ((angle) / 180.0 * M_PI)

@implementation LevelEditorViewController

@synthesize buildingTypeMVC, editorOptionsVC, groundModalVC;

#pragma mark -- Initializations

- (void)createNewZone
{
    [[EditorManager sharedManager] setCurrentObject:nil];
    [[EditorManager sharedManager] initWithNewZone];
    [[CameraManager sharedManager] resetCamera];
}

- (void)loadZone:(ObstacleZone *)zone
{
    [[EditorManager sharedManager] setCurrentObject:nil];
    [[EditorManager sharedManager] loadZone:zone];
    [[CameraManager sharedManager] resetCamera];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[RenderManager sharedManager] context];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [self setupGL];
    [self initializeTransformValues];
    [self initializeGestureRecognizers];
    [self initializeButtons];
}

- (void)initializeButtons
{
    [leftRightButton setImage:[UIImage imageNamed:@"move-left-right.tiff"] forState:UIControlStateNormal];
    [leftRightButton setImage:[UIImage imageNamed:@"move-left-right-touched.tiff"] forState:UIControlStateHighlighted];
    [forwardBackButton setImage:[UIImage imageNamed:@"forward-back.tiff"] forState:UIControlStateNormal];
    [forwardBackButton setImage:[UIImage imageNamed:@"forward-back-touched.tiff"] forState:UIControlStateHighlighted];
    [rotateButton setImage:[UIImage imageNamed:@"rotate-icon.tiff"] forState:UIControlStateNormal];
    [rotateButton setImage:[UIImage imageNamed:@"rotate-touched-icon.tiff"] forState:UIControlStateHighlighted];
    [deleteButton setImage:[UIImage imageNamed:@"delete-icon.tiff"] forState:UIControlStateNormal];
    [deleteButton setImage:[UIImage imageNamed:@"delete-touched-icon.tiff"] forState:UIControlStateHighlighted];
    
}

- (void)initializeTransformValues
{
    // new
    currentToolType = -1;
}

- (void)initializeGestureRecognizers
{
    /*
     *  Pinch Gesture
     */
    UIPinchGestureRecognizer *twoFingerPinch =
    [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerPinch:)];
    [[self view] addGestureRecognizer:twoFingerPinch];
    
    /*
     *  Long Press Gesture
     */
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:
                                                 @selector(longPress:)];
    longPress.minimumPressDuration = 2;
    longPress.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:longPress];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
	self.context = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

- (void)setupGL
{
    
    [EAGLContext setCurrentContext:self.context];
    /*
     * Initialize camera to look at the first object
     */
    currentCamera = OBJECT_CAMERA;
    
    glEnable(GL_DEPTH_TEST);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
}


#pragma mark - GLKView and GLKViewController delegate methods


- (void)update
{
    //update Camera
    CGFloat scale = UIScreen.mainScreen.scale;
    GLsizei w = self.view.bounds.size.width * scale;
    GLsizei h = self.view.bounds.size.height * scale;
    [CameraManager sharedManager].aspect = fabsf(w / h);
    [[CameraManager sharedManager] update];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    CGFloat scale = UIScreen.mainScreen.scale;
    GLsizei w = self.view.bounds.size.width * scale;
    GLsizei h = self.view.bounds.size.height * scale;
    glViewport(0, 0, w, h);
    glClearColor(0.1f, 0.1f, 0.7f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    [[EditorManager sharedManager] render];
}

#pragma mark - Object creation

- (void)buildingButtonPressed:(id)sender
{
    if (self.buildingTypeMVC == nil)
    {
        self.buildingTypeMVC = [[BuildingTypeModalViewController alloc] initWithNibName:@"BuildingTypeModalViewController" bundle:nil];
    }
    
    [self presentModalViewController:self.buildingTypeMVC animated:YES];
}

- (void)groundButtonPressed:(id)sender
{
    if (self.groundModalVC == nil)
    {
        self.groundModalVC = [[GoundTypeModalViewController alloc] initWithNibName:@"GoundTypeModalViewController" bundle:nil];
    }
    
    [self presentModalViewController:self.groundModalVC animated:YES];
}

- (void)spawnButtonPressed:(id)sender
{
    [[EditorManager sharedManager] addMissleSpawn];
    [[CameraManager sharedManager] objectCamera];
}

- (void)tunnelButtonPressed:(id)sender
{
    [[EditorManager sharedManager] addTunnel];
    [[CameraManager sharedManager] objectCamera];
}

#pragma mark - Object transformation

- (void)transformObject:(float)value
{
    if ([[EditorManager sharedManager] currentObject] == nil)
        return;
    
    switch (currentToolType)
    {
        case LEFT_RIGHT_TOOL:
        {
            [self translateCurrentObject:value];
            break;
        }
        case FORWARD_BACK_TOOL:
        {
            [self translateCurrentObject:value];
            break;
        }
        case ROTATE_TOOL:
        {
            [self rotateCurrentObject:(value * 3.0)];
            break;
        }
    }
}

- (void)translateCurrentObject:(float)value
{
    glm::vec3 curTranslateVal = [[[EditorManager sharedManager] currentObject] position];
    glm::vec3 newTranslateVal =
    glm::vec3(currentToolType == LEFT_RIGHT_TOOL ? curTranslateVal.x + value : curTranslateVal.x,
              curTranslateVal.y,
              currentToolType == FORWARD_BACK_TOOL ? curTranslateVal.z + value : curTranslateVal.z);
    [[[EditorManager sharedManager] currentObject] setPosition:newTranslateVal];
}

- (void)rotateCurrentObject:(float)value
{
    glm::vec3 curRotateVal = [[[EditorManager sharedManager] currentObject] rotation];
    glm::vec3 newRotateVal =
    glm::vec3(curRotateVal.x,
              currentToolType == ROTATE_TOOL ? curRotateVal.y + value : curRotateVal.y,
              curRotateVal.z);
    [[[EditorManager sharedManager] currentObject] setRotation:newRotateVal];
}

/*
- (void)scaleCurrentObject:(float)value
{
    glm::vec3 curSizeVal = [[[EditorManager sharedManager] currentObject] size];
    glm::vec3 newSizeVal =
    glm::vec3(currentDirection == XDIR ? curSizeVal.x + value : curSizeVal.x,
              currentDirection == YDIR ? curSizeVal.y + value : curSizeVal.y,
              currentDirection == ZDIR ? curSizeVal.z + value : curSizeVal.z);
    [[[EditorManager sharedManager] currentObject] setSize:newSizeVal];
}
*/


#pragma mark - New button action methods

- (IBAction)leftRightButtonPressed:(id)sender
{
    // Button highlighting code
    [currentToolButton setHighlighted:NO];
    if (currentToolButton == leftRightButton)
    {
        currentToolButton = nil;
        currentToolType = -1;
    }
    else
    {
        currentToolButton = leftRightButton;
        [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentToolButton setHighlighted:YES]; }];
        currentToolType = LEFT_RIGHT_TOOL;
    }
}

- (IBAction)forwardBackButtonPressed:(id)sender
{
    // Button highlighting code
    [currentToolButton setHighlighted:NO];
    if (currentToolButton == forwardBackButton)
    {
        currentToolButton = nil;
        currentToolType = -1;
    }
    else
    {
        currentToolButton = forwardBackButton;
        [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentToolButton setHighlighted:YES]; }];
        currentToolType = FORWARD_BACK_TOOL;
    }
}

- (void)rotateButtonPressed:(id)sender
{
    [currentToolButton setHighlighted:NO];
    if (currentToolButton == rotateButton)
    {
        currentToolButton = nil;
        currentToolType = -1;
    }
    else
    {
        currentToolButton = rotateButton;
        [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentToolButton setHighlighted:YES]; }];
        currentToolType = ROTATE_TOOL;
    }
}

- (void)deleteButtonPressed:(id)sender
{
    [deleteButton setHighlighted:YES];
    
    if ([[EditorManager sharedManager] currentObject] != nil)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Delete"
                              message:@"Are you sure you want to delete the current object?"
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Delete", nil];
        [alert show];
    }
}

#pragma mark - Alert View delegate method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Delete"])
    {
        [[EditorManager sharedManager] deleteCurrentObject];
    }
}

#pragma mark - Camera

- (void)cameraButtonPressed:(id)sender
{
    currentCamera = (currentCamera == OBJECT_CAMERA) ? OVERHEAD_CAMERA :OBJECT_CAMERA;
    
    switch (currentCamera)
    {
        case OBJECT_CAMERA:
        {
            [[CameraManager sharedManager] objectCamera];
            [cameraButton setBackgroundImage:[UIImage imageNamed:@"objectCamera-icon.tiff"] forState:nil];
            break;
        }
        case OVERHEAD_CAMERA:
        {
            [[CameraManager sharedManager] overheadCamera];
            [cameraButton setBackgroundImage:[UIImage imageNamed:@"overheadCamera-icon.tiff"] forState:nil];
            break;
        }
    }
}

#pragma mark - Options Button

- (void)optionsButtonPressed:(id)sender
{
    if (self.editorOptionsVC == nil)
    {
        self.editorOptionsVC = [[EditorOptionsViewController alloc] initWithNibName:@"EditorOptionsViewController" bundle:nil];
    }
    [self presentModalViewController:self.editorOptionsVC animated:YES];
}

#pragma mark - Gesture Recognizer Methods

- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer
{
    switch (currentCamera)
    {
        case OBJECT_CAMERA:
        {
            if (recognizer.scale < 1)
            {
                float radius = [CameraManager sharedManager].radius + 0.5f;
                [[CameraManager sharedManager] setZoom:radius];
            }
            else if (recognizer.scale >= 1)
            {
                float radius = [CameraManager sharedManager].radius - 0.5f;
                [[CameraManager sharedManager] setZoom:radius];
            }
            break;
        }
        case OVERHEAD_CAMERA:
        {
            if (recognizer.scale < 1)
            {
                //TODO: zoom capabilities for overhead camera
            }
            else if (recognizer.scale >= 1)
            {
                
            }
            break;
        }
    }
}

- (void)longPress:(UILongPressGestureRecognizer *)recognizer
{
    NSLog(@"Long press detected");
}

#pragma mark - Touch events


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count < 2)
    {
        touchType = SELECTION;
        for (UITouch *touch in touches)
        {
            CGPoint loc = [touch locationInView:self.view];
            touchStartPos = loc;
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touches.count < 2)
    {
        touchType = MOTION;
        for (UITouch *touch in touches)
        {
            CGPoint loc = [touch locationInView:self.view];
            if (currentToolType == -1)
            { // 3d rotation code
                if (currentCamera == OBJECT_CAMERA)
                { // of perform rotation if in object camera
                    float sensitivity = 0.05;
                    float theta = DEG_2_RAD((loc.x - touchStartPos.x) * sensitivity);
                    float phi = DEG_2_RAD((loc.y - touchStartPos.y) * sensitivity);
                    [[CameraManager sharedManager] setTheta:theta andPhi:phi];
                }
            }
            else
            { // hand gesture transformation code
                if (currentCamera == OVERHEAD_CAMERA)
                {
                    [[CameraManager sharedManager] overheadCamera];
                }
                
                touchMovedPos = loc;
                float sensitivity = (0.005);
                float offset = (touchMovedPos.x - touchStartPos.x) * sensitivity;
                [self transformObject:offset];
                
                if (currentCamera == OBJECT_CAMERA)
                {
                    [[CameraManager sharedManager] followObject];
                }
            }
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        if (touchType == SELECTION)
        {
            //TODO: Read pixels successfully
            [self detectSelectedObject:touchStartPos];
            touchType = -1;
        }
        else if (touchType == MOTION)
        {
            
        }
        [[CameraManager sharedManager] setShouldUpdateRotation:NO];
    }
}

#pragma mark -- OpenGL Selection

- (void)detectSelectedObject:(CGPoint)screenCoord
{
    GameObject *selectObject = [self selectObjectAtPos:screenCoord];
    
    // set camera to selected object
    if (selectObject != nil)
    {
        [selectObject.bbox setIntersection:YES];
        [[EditorManager sharedManager] setCurrentObject:selectObject];
        [[CameraManager sharedManager] objectCamera];
        currentCamera = OBJECT_CAMERA;
    }
    
}

- (GameObject*)selectObjectAtPos:(CGPoint)loc
{
    PickingRay *p = [[PickingRay alloc] initWithView:self.view screenPoint:loc];
    
    glm::vec3 camPos = [[CameraManager sharedManager] position];
    GameObject *closestObject = nil;
    float minDistance = 100;
    
    // do ray intersection in x y z for all objects with bounding boxes in the scene
    
    NSMutableArray *gameObjects = [[[EditorManager sharedManager] obstacleZone] objectsArray];
    for (GameObject *g in gameObjects)
    {
        glm::vec3 differnce = g.position - camPos;
        float distance = (float)sqrt(differnce.x*differnce.x + differnce.y+differnce.y + differnce.z*differnce.z);
        if ([p testZIntersection:g] && distance < minDistance)
        {
            minDistance = distance;
            closestObject = g;
        }
        else if ([p testXIntersection:g] && distance < minDistance)
        {
            minDistance = distance;
            closestObject = g;
        }
        else if ([p testYIntersection:g] && distance < minDistance)
        {
            minDistance = distance;
            closestObject = g;
        }
    }
    
    return closestObject;
}

@end
