//
//  MainScreenViewController.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "MainScreenViewController.h"

@implementation MainScreenViewController

@synthesize zoneListViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [RenderManager sharedManager];
        [self initResources];
    }
    return self;
}

- (void)initResources
{
    [EAGLContext setCurrentContext:[RenderManager sharedManager].context];
    
    // Set up managers and initialize resources for game
    [CameraManager sharedManager];
    [MeshManager   sharedManager];
    [TextureManager sharedManager];
    [EditorManager  sharedManager];
    [ZoneListManager sharedManager];
    [DataManager sharedManager];
    [WorldManager sharedManager];
    [GyroManager sharedManager];
    [VFCulling sharedManager];
    [AudioManager sharedManager];
}

#pragma mark - Orientation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

#pragma mark - Button Events

- (void)editZonesButtonPressed:(id)sender
{
    if (self.zoneListViewController == nil)
    {
        self.zoneListViewController = [[ZoneListViewController alloc] initWithNibName:@"ZoneListViewController" bundle:nil];
    }
    
    [self presentModalViewController:self.zoneListViewController animated:YES];
}

- (void)playGameButtonPressed:(id)sender
{
    if (self.gameViewController == nil)
    {
        self.gameViewController = [[GameViewController alloc] initWithNibName:@"GameViewController" bundle:nil];
    }
    [self presentModalViewController:self.gameViewController animated:YES];
    [[WorldManager sharedManager] startGame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
