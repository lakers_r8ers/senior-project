//
//  EditorOptionsViewController.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditorManager.h"
#import "ZoneListManager.h"
#import "DataManager.h"

@interface EditorOptionsViewController : UIViewController
<UITextFieldDelegate>
{
    IBOutlet UITextField *nameTextField;
    IBOutlet UILabel *errorMessage;
}

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)quitButtonPressed:(id)sender;
- (IBAction)backgroundTouched:(id)sender;

@end
