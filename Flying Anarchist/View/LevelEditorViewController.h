//
//  LevelEditorViewController.h
//  Flying Anarchist
//
//  Created by Lion User on 12/20/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "CheckGLErrors.h"
#import "CameraManager.h"
#import "EditorManager.h"
#import "TextureManager.h"
#import "BuildingTypeModalViewController.h"
#import "EditorOptionsViewController.h"
#import "RenderManager.h"
#import "PickingRay.h"
#import "GoundTypeModalViewController.h"

enum CAMERA_TYPE
{
    OBJECT_CAMERA, OVERHEAD_CAMERA
};

enum Touch_Type
{
    SELECTION, MOTION
};

enum Tool_Type
{
    LEFT_RIGHT_TOOL, FORWARD_BACK_TOOL, ROTATE_TOOL
};

@interface LevelEditorViewController : GLKViewController
<UIAlertViewDelegate>
{
    IBOutlet UIButton *cameraButton;
    
    //---- toolbar buttons
    IBOutlet UIButton *leftRightButton;
    IBOutlet UIButton *forwardBackButton;
    IBOutlet UIButton *rotateButton;
    IBOutlet UIButton *deleteButton;
    
    UIButton *currentToolButton;
    int currentToolType;
    int currentCamera;
    
    BuildingTypeModalViewController *buildingTypeMVC;
    
    //touch transform variables
    CGPoint touchStartPos;
    CGPoint touchMovedPos;
    int touchType;
    
    //IBOutlet UIImageView *imageView;
}

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;
@property (strong, nonatomic) BuildingTypeModalViewController *buildingTypeMVC;
@property (strong, nonatomic) EditorOptionsViewController *editorOptionsVC;
@property (strong, nonatomic) GoundTypeModalViewController *groundModalVC;

- (void)createNewZone;
- (void)loadZone:(ObstacleZone *)zone;

- (void)setupGL;
- (void)tearDownGL;

- (IBAction)buildingButtonPressed:(id)sender;
- (IBAction)groundButtonPressed:(id)sender;
- (IBAction)spawnButtonPressed:(id)sender;
- (IBAction)tunnelButtonPressed:(id)sender;

- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)optionsButtonPressed:(id)sender;

// New Object transformation actions
- (IBAction)leftRightButtonPressed:(id)sender;
- (IBAction)forwardBackButtonPressed:(id)sender;
- (IBAction)rotateButtonPressed:(id)sender;
- (IBAction)deleteButtonPressed:(id)sender;


@end
