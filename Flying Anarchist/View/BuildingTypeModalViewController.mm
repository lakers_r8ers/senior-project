//
//  BuildingTypeModalViewController.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "BuildingTypeModalViewController.h"

@implementation BuildingTypeModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentBuildingType = -1;
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone &&
        (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)) {
        return YES;
    }
    return NO;
}

- (void)doneButtonPressed:(id)sender
{
    [[EditorManager sharedManager] addBuilding:currentBuildingType];
    [[CameraManager sharedManager] objectCamera];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)cancelButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)skyscraper1ButtonPressed:(id)sender
{
    currentBuildingType = Skyscraper1;
    [currentButton setHighlighted:NO];
    currentButton = skyscraper1Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)skyscraper2ButtonPressed:(id)sender
{    
    currentBuildingType = Skyscraper2;
    [currentButton setHighlighted:NO];
    currentButton = skyscraper2Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)skyScraper3ButtonPressed:(id)sender
{
    currentBuildingType = Skyscraper3;
    [currentButton setHighlighted:NO];
    currentButton = skyscraper3Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)skyscraper4ButtonPressed:(id)sender
{
    currentBuildingType = Skyscraper4;
    [currentButton setHighlighted:NO];
    currentButton = skyscraper4Button;
    [NSOperationQueue.mainQueue addOperationWithBlock:^{ [currentButton setHighlighted:YES]; }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
