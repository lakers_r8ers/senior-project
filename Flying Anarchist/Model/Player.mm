//
//  Player.m
//  Level Editor
//
//  Created by Lion User on 04/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "Player.h"

@implementation Player

@synthesize speed;
@synthesize health;
@synthesize animationState;

#pragma mark -- Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        [self initializePlayer];
    }
    return self;
}

- (void)initializePlayer
{
    position = glm::vec3(0, 0, 0);
    direction = glm::vec3(0, 0, 1);
    size = glm::vec3(0.5f, 0.5f, 0.5f);
    speed = 2.0;
    health = ALIVE;
    
    //initialize shader
    shader = [[ShaderManager sharedManager]
              createShader:@"ToonShader" withFile:@"ToonShader.plist"];
    
    //initialize player mesh with a shader
    mesh = [[MeshManager sharedManager] meshForName:@"ship.obj"];
    [mesh loadWithShader:shader];
    
    // initialize texture
    texture = [[TextureManager sharedManager] textureForName:@"ship.png"];
    
    // initialize bbox
    bbox = [[BoundingBox alloc] init];
    bboxOffset = glm::vec3(0.f, -0.35f, 0.f);
    
    animationState = STABLE;
}

#pragma mark -- Update

- (void)update
{
    [[GyroManager sharedManager] update];
    [self updatePlayerPos];
    [self setPlayerRotationBasedOnHeight];
}

- (void)updatePlayerPos
{
    if (health == ALIVE)
    {
        // updates player z direction
        position.z += (speed);
    
        // update player x direction
        float pitch = [[GyroManager sharedManager] pitch];
        rotation.z = -pitch;
        [self rotationToTranslationX:pitch];
        [self checkPlayerHeight];
    }
}

- (void)rotationToTranslationX:(float)value
{ // move player left and right based on gyroscope info
    if (value > 3.0)
    {
        float offset = value / 45.f;
        position.x += (1.f * fabs(offset));
    }
    else if (value < -3.0)
    {
        float offset = value / 45.f;
        position.x -= (1.f * fabs(offset));
    }
    
    
}

#pragma mark - dodging code

- (void)setPlayerHeight:(float)height
{ 
    if (health == DEAD)
        return;
    
    if ((position.y + height) <= MIN_HEIGHT || (position.y + height) >= MAX_HEIGHT)
    {
        if (rotation.x != 0.f)
        {
            rotation.x += (animationState == MOVING_DOWN) ? -1.f : 1.f;
        }
        return;
    }

    position.y += height;
    animationState = (height < 0) ? MOVING_DOWN : MOVING_UP;
}

- (void)checkPlayerHeight
{ // ranges player height between 1 and 10
    if (animationState == STABLE && position.y > STABLE_HEIGHT)
    {
        if (position.y - STABLE_HEIGHT < 0.5)
        {
            position.y = 5.f;
            return;
        }
        position.y -= 0.5f;
    }
    else if (animationState == STABLE && position.y < STABLE_HEIGHT)
    {
        if (STABLE_HEIGHT - position.y < 0.5)
        {
            position.y = 5.f;
            return;
        }
        position.y += 0.5f;
    }
}

- (void)setPlayerRotationBasedOnHeight
{ // sets the rotation value of player to be based on the player height
  // represents the rotation animation when the players height changes
    float rotation_limit = 15.f;
    if (position.y > 1 && position.y <= 2.5f)
    {
        float rotationDelta = (position.y * (rotation_limit / 2.5f)) - 6.f;
        rotation.x = (animationState == MOVING_UP || animationState == STABLE) ?
                     -rotationDelta : rotationDelta;
    }
    else if (position.y > 2.5 && position.y <= 5.0)
    {
        float rotationDelta = (position.y * (rotation_limit / 2.5f)) - 30.f;
        rotation.x = (animationState == MOVING_UP || animationState == STABLE) ?
                     rotationDelta : -rotationDelta;
    }
    else if (position.y > 5.0 && position.y <= 7.5)
    {
        float rotationDelta = (position.y * (rotation_limit / 2.5f)) - 30.f;
        rotation.x = (animationState == MOVING_DOWN || animationState == STABLE) ?
                     rotationDelta : -rotationDelta;
    }
    else if (position.y > 7.5 && position.y <= 10.0)
    {
        float rotationDelta = (position.y * (rotation_limit / 2.5f)) - 60.f;
        rotation.x = (animationState == MOVING_DOWN || animationState == STABLE) ?
                     -rotationDelta : rotationDelta;
    }
}


#pragma mark -- Render

- (void)render
{
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    modelMatrix = glm::rotate(modelMatrix, 90.f, glm::vec3(0.f, 1.f, 0.f));
    
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    //calculate normal matrix
    glm::mat4 modelViewMatrix = [[CameraManager sharedManager] viewMatrix] * modelMatrix;
    normalMatrix = glm::transpose(glm::inverse(glm::mat3x3(modelViewMatrix)));
    
    // update bounding box
    [self updateBBox];
    
    [[ShaderManager sharedManager] useShader:@"ToonShader"];
    
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix3fv([shader uniformLocation:@"normalMatrix"], 1, GL_FALSE, &normalMatrix[0][0]);
    glUniform1fv([shader uniformLocation:@"camPos"], 3, glm::value_ptr([[CameraManager sharedManager] position]));
    
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
}

#pragma mark - bounding box methods

- (void)renderBBox
{
    [bbox render];
}

- (void)updateBBox
{
    [bbox setSize:(size + bboxOffset)];
    [bbox setRotation:rotation];
    [bbox setPosition:position];
}

@end
