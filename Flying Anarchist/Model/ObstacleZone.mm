//
//  ObstacleZone.m
//  Level Editor
//
//  Created by Lion User on 04/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "ObstacleZone.h"

@implementation ObstacleZone

@synthesize name, objectsArray, ground, mode;

- (id)init
{
    self = [super init];
    if (self)
    {
        objectsArray = [[NSMutableArray alloc] init];
        ground = [[Ground alloc] initWithGroundType:GROUND1];
        mode = EDITING;
    }
    return self;
}

#pragma mark -- NSCoding protocol methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:name forKey:@"name"];
    [aCoder encodeObject:objectsArray forKey:@"objectsArray"];
    [aCoder encodeObject:ground forKey:@"ground"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        name = [aDecoder decodeObjectForKey:@"name"];
        objectsArray = [aDecoder decodeObjectForKey:@"objectsArray"];
        ground = [aDecoder decodeObjectForKey:@"ground"];
        mode = EDITING;
    }
    return self;
}

- (void)setZOffset:(float)offset
{
    for (GameObject *g in objectsArray)
    {
        [g setPosition:(glm::vec3(g.position.x,
                                  g.position.y,
                                  g.position.z + offset))];
        [g.bbox setPosition:(glm::vec3(g.bbox.position.x,
                                       g.bbox.position.y,
                                       g.bbox.position.z + offset))];
    }
    [ground setPosition:(glm::vec3(ground.position.x,
                                   ground.position.y,
                                   ground.position.z + offset))];
    [ground.bbox setPosition:(glm::vec3(ground.bbox.position.x,
                                        ground.bbox.position.y,
                                        ground.bbox.position.z + offset))];
}

- (void)update
{
    
}

- (void)changeGroundToType:(int)type
{
    [ground changeGroundToType:type];
}

- (void)addGameObject:(GameObject *)object
{
    [objectsArray addObject:object];
}

- (void)deleteGameObject:(GameObject *)object
{
    [objectsArray removeObject:object];
}

- (void)render
{
    // view frustrum culling setup
    
    for (GameObject *g in objectsArray)
    {
        //if ([[VFCulling sharedManager] boxInFrustrum:g.bbox] == OUTSIDE)
        //    continue;
        
        switch ([g objectType])
        {
            case BUILDING:
            {
                [g render];
                //[g renderBBox];
                break;
            }
            case MISSLE:
            {
                MissleSpawn *missle = (MissleSpawn *)g;
                [missle render];
                //[missle renderBBox];
                
                if (missle.hasDirection)
                {
                    [missle fireMissle];
                }
                break;
            }
            case TUNNEL:
            {
                [g render];
                //[g renderBBox];
            }
        }
    }
    [ground render];
}

- (void)reset
{
    for (GameObject *g in objectsArray)
    {
        [g reset];
    }
}

@end
