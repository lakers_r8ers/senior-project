//
//  Skybox.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "GameObject.h"
#import "glm.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "TextureManager.h"
#import "TextureUtil.h"
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "CameraManager.h"
#import "CheckGLErrors.h"

@interface Skybox : GameObject
{
    glm::mat4 modelMatrix;
    glm::mat3 normalMatrix;
    
    TextureUtil *texture;
    MeshUtil *mesh;
    ShaderUtil *shader;
}

- (void)update;
@end
