//
//  Tunnel.h
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/30/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "GameObject.h"
#import "glm.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "TextureManager.h"
#import "TextureUtil.h"
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "CameraManager.h"
#import "CheckGLErrors.h"
#import "EditorManager.h"
#import "BoundingBox.h"

@interface Tunnel : GameObject
<NSCoding>
{
    MeshUtil *mesh;
    ShaderUtil *shader;
    TextureUtil *texture;
    glm::mat4 modelMatrix;
    //glm::vec3 bbox_offset;
    std::vector<glm::vec3> bboxOffsetList;
}

- (void)update;

@end
