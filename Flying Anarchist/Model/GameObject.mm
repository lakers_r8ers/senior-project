//
//  GameObject.m
//  Level Editor
//
//  Created by Lion User on 04/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "GameObject.h"

@implementation GameObject

@synthesize position;
@synthesize direction;
@synthesize size;
@synthesize rotation;
@synthesize bbox;
@synthesize objectType;
@synthesize bboxList;
@synthesize bboxType;

static float genColor[] = {0, 0, 0};

#pragma mark -- Initialization methods
- (id)init
{
    self = [super init];
    if (self)
    {
        position = glm::vec3(0, 0, 0);
        direction = glm::vec3(0, 0, 0);
        size = glm::vec3(1.f);
        rotation = glm::vec3(0, 0, 0);
    }
    return self;
}

- (void)initColor
{
    colorID = glm::vec3(genColor[0] / 255, genColor[1] / 255, genColor[2] / 255);
    
    genColor[0]++;
    if (genColor[0] > 255)
    {
        genColor[0] = 0;
        genColor[1]++;
        if (genColor[1] > 255)
        {
            genColor[1] = 0;
            genColor[2]++;
        }
    }
    //printf("ColorID (%f, %f, %f)\n", colorID.r, colorID.g, colorID.b);
}

#pragma mark -- NSCoding protocol methods

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[NSData dataWithBytes:glm::value_ptr(position) length:(3 * sizeof(float))] forKey:@"position"];
    [aCoder encodeObject:[NSData dataWithBytes:glm::value_ptr(direction) length:3] forKey:@"direction"];
    [aCoder encodeObject:[NSData dataWithBytes:glm::value_ptr(size) length:(3 * sizeof(float))] forKey:@"size"];
    [aCoder encodeObject:[NSData dataWithBytes:glm::value_ptr(rotation) length:(3 * sizeof(float))] forKey:@"rotation"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    NSData *tempData;
    float vec3[3];
    
    if (self = [super init])
    {
        //decode position
        tempData = [aDecoder decodeObjectForKey:@"position"];
        [tempData getBytes:vec3 length:(3 * sizeof(float))];
        position = glm::make_vec3(vec3);
        
        //decode direction
        tempData = [aDecoder decodeObjectForKey:@"direction"];
        [tempData getBytes:vec3 length:3];
        direction = glm::make_vec3(vec3);
        
        //size
        tempData = [aDecoder decodeObjectForKey:@"size"];
        [tempData getBytes:vec3 length:(3 * sizeof(float))];
        size = glm::make_vec3(vec3);
        
        //rotation
        tempData = [aDecoder decodeObjectForKey:@"rotation"];
        [tempData getBytes:vec3 length:(3 * sizeof(float))];
        rotation = glm::make_vec3(vec3);
    }
    return self;
}

- (void)renderBBox
{
    
}

- (void)render
{
    
}

- (void)picking
{
    
}

- (void)reset
{
    
}
@end
