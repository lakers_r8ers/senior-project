//
//  GameObject.h
//  Level Editor
//
//  Created by Lion User on 04/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "glm.hpp"
#import "type_ptr.hpp"
#import "matrix_transform.hpp"
#import <vector>

enum OBJECT_TYPES
{
    BUILDING, MISSLE, SPAWN, TUNNEL
};
enum BOUNDING_BOX_TYPE
{
  SINGLE_BBOX, BBOX_LIST
};

@class BoundingBox;
@interface GameObject : NSObject
<NSCoding>
{
    glm::vec3 position;
    glm::vec3 direction;
    glm::vec3 size;
    glm::vec3 rotation;
    glm::vec3 colorID;
    BoundingBox *bbox;
    NSMutableArray *bboxList;
    int objectType;
    int bboxType;
}

@property (nonatomic) glm::vec3 position;
@property (nonatomic) glm::vec3 direction;
@property (nonatomic) glm::vec3 size;
@property (nonatomic) glm::vec3 rotation;
@property (nonatomic) BoundingBox *bbox;
@property (nonatomic) int objectType;
@property (nonatomic) NSMutableArray *bboxList;;
@property (nonatomic) int bboxType;

- (void)reset;
- (void)render;
- (void)renderBBox;
- (void)picking;

@end
