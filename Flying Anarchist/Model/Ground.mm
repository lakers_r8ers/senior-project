//
//  Ground.m
//  Flying Anarchist
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "Ground.h"

@implementation Ground

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        position = glm::vec3(0.0, 0.0, 0.0);
        modelMatrix = glm::mat4(1.f);
        size = glm::vec3(100.f, 0.f, 100.f);
        shader = [[ShaderManager sharedManager] createShader:@"GroundShader" withFile:@"Ground.plist"];
        
        mesh = [[MeshManager sharedManager] meshForName:@"ground.obj"];
        [mesh loadWithShader:shader];
        
        texture = [[TextureManager sharedManager] textureForName:@"ground.png"];
        bbox = [[BoundingBox alloc] init];
        bboxType = SINGLE_BBOX;
    }
    return self;
}

- (id)initWithGroundType:(int)type
{
    if ((self = [super init]))
    {
        [self setGroundType:type];
    }
    
    return self;
}

- (void)setGroundType:(int)type
{
    position = glm::vec3(0.0, 0.0, 0.0);
    size = glm::vec3(100.f, 0.f, 100.f);
    modelMatrix = glm::mat4(1.f);
    
    shader = [[ShaderManager sharedManager] createShader:@"GroundShader" withFile:@"Ground.plist"];
    
    mesh = [[MeshManager sharedManager] meshForName:@"ground.obj"];
    [mesh loadWithShader:shader];
    
    bbox = [[BoundingBox alloc] init];
    bboxType = SINGLE_BBOX;
    
    [self changeGroundToType:groundType];
    
}

- (void)changeGroundToType:(int)type
{
    groundType = type;
    switch (groundType)
    {
        case GROUND1:
        {
            texture = [[TextureManager sharedManager] textureForName:@"ground.png"];
            break;
        }
        case GROUND2:
        {
            texture = [[TextureManager sharedManager] textureForName:@"ground2.png"];
            break;
        }
        case GROUND3:
        {
            texture = [[TextureManager sharedManager] textureForName:@"ground3.png"];
            break;
        }
        case GROUND4:
        {
            texture = [[TextureManager sharedManager] textureForName:@"ground4.png"];
            break;
        }
    }
}

#pragma mark -- NSCoding protocol methods
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:groundType forKey:@"groundType"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        groundType = [aDecoder decodeIntForKey:@"groundType"];
        [self setGroundType:groundType];
    }
    return self;
}

- (void)update
{
    
}

- (void)render
{
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, 0.f, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    //calculate normal matrix
    glm::mat4 modelViewMatrix = [[CameraManager sharedManager] viewMatrix] * modelMatrix;
    normalMatrix = glm::transpose(glm::inverse(glm::mat3x3(modelViewMatrix)));
    
    // update bbox
    [self updateBBox];
    
    [[ShaderManager sharedManager] useShader:@"GroundShader"];
    
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix3fv([shader uniformLocation:@"normalMatrix"], 1, GL_FALSE, &normalMatrix[0][0]);
    
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
}

- (void)renderBBox
{
    [bbox render];
}

- (void)updateBBox
{
    [bbox setPosition:position];
    [bbox setRotation:rotation];
    [bbox setSize:size];
}

@end
