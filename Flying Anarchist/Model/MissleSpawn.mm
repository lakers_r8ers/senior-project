//
//  MissleSpawn.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "MissleSpawn.h"

@implementation MissleSpawn

@synthesize speed;
@synthesize hasDirection;

- (id)init
{
    self = [super init];
    if (self)
    {
        position = glm::vec3(0.0, 5.0, 0.0);
        [self setupMissle];
    }
    return self;
}

- (void)setupMissle
{
    objectType = MISSLE;
    modelMatrix = glm::mat4(1.f);
    size = glm::vec3(10.f, 10.f, 10.f);
    shader = [[ShaderManager sharedManager] createShader:@"SpawnArrow" withFile:@"SpawnArrow.plist"];
    
    mesh = [[MeshManager sharedManager] meshForName:@"missle.obj"];
    [mesh loadWithShader:shader];
    
    texture = [[TextureManager sharedManager] textureForName:@"missle.png"];
    bbox = [[BoundingBox alloc] init];
    bbox_offset = glm::vec3(-7.f, -7.f, 0.f);
    bboxType = SINGLE_BBOX;
    speed = 0.f;
    hasDirection = NO;
    orientationAxis = glm::vec3(1.f);
    orientationAngle = 0.f;
    [bbox setHasOrientation:NO];
}

- (void)reset
{
    hasDirection = NO;
    orientationAxis = glm::vec3(1.f);
    orientationAngle = 0.f;
    [bbox setHasOrientation:NO];
}

- (void)fireMissle
{
    position += (direction * speed);
}

- (void)orientMissleWithPlayerPos:(glm::vec3)pos
{
    glm::vec3 missleToPlayerDir = direction;
    float dot = glm::dot(missleToPlayerDir, position) / (glm::length(missleToPlayerDir) * glm::length(position));
    orientationAngle = (acos(dot) * 180) / M_PI;
    orientationAxis = glm::cross(missleToPlayerDir, position);
    [bbox setHasOrientation:YES];
    [bbox setOrientationAngle:orientationAngle];
    [bbox setAxis:orientationAxis];
}

#pragma mark -- NSCoding protocol methods
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setupMissle];
    }
    return self;
}

- (void)update
{
    
}

- (void)render
{
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    if ([bbox hasOrientation])
    {
        modelMatrix = glm::rotate(modelMatrix, (-orientationAngle), glm::vec3(0, orientationAxis.y, 0));
    }
    else
    {
        modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
        modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
        modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
        modelMatrix = glm::rotate(modelMatrix, -orientationAngle, glm::vec3(orientationAxis.x, 0, 0));
        modelMatrix = glm::rotate(modelMatrix, 180.f, glm::vec3(0.f, 1.f, 0.f));
    }
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    // update bounding box
    [self updateBBox];
    
    //Use shader
    [[ShaderManager sharedManager] useShader:@"SpawnArrow"];
    
    //Pass values to shader
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    
    //Pass texture to shader
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
}

#pragma mark - BBox methods

- (void)updateBBox
{
    [bbox setSize:(size + bbox_offset)];
    [bbox setRotation:rotation];
    [bbox setPosition:position];
}

- (void)renderBBox
{
    [bbox render];
}

@end
