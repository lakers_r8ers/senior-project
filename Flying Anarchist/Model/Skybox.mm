//
//  Skybox.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 2/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "Skybox.h"

@implementation Skybox
- (id)init
{
    self = [super init];
    if (self)
    {
        position = glm::vec3(0.0, 15.0, 0.0);
        modelMatrix = glm::mat4(1.f);
        size = glm::vec3(150.f, 150.f, 150.f);
        shader = [[ShaderManager sharedManager] createShader:@"Skybox" withFile:@"Skybox.plist"];
        
        mesh = [[MeshManager sharedManager] meshForName:@"skybox.obj"];
        [mesh loadWithShader:shader];
        
        texture = [[TextureManager sharedManager] textureForName:@"skybox.png"];
        
    }
    return self;
}

- (void)update
{
    
}

- (void)render
{
    static float cloud_movement = 0;
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, 90.f + cloud_movement, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, 90.f, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    cloud_movement += 0.03;
    
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    //calculate normal matrix
    glm::mat4 modelViewMatrix = [[CameraManager sharedManager] viewMatrix] * modelMatrix;
    normalMatrix = glm::transpose(glm::inverse(glm::mat3x3(modelViewMatrix)));
    
    //Use shader
    [[ShaderManager sharedManager] useShader:@"Skybox"];
    
    //Pass values to shader
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix3fv([shader uniformLocation:@"normalMatrix"], 1, GL_FALSE, &normalMatrix[0][0]);
    
    //Pass texture to shader
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
}
@end
