//
//  Building.m
//  Flying Anarchist
//
//  Created by Lion User on 09/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "Building.h"

@implementation Building

- (id)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}


- (id)initWithBuildingType:(int)type
{
    if ((self = [super init]))
    {
        [self setBuildingType:type];
    }
    return self;
}

- (void)setBuildingType:(int)type
{
    bbox = [[BoundingBox alloc] init];
    modelMatrix = glm::mat4(1.f);
    shader = [[ShaderManager sharedManager] createShader:@"TestShader" withFile:@"TestShader.plist"];
    buildingType = type;
    objectType = BUILDING;
    bboxType = SINGLE_BBOX;
    
    //Initializes building based on the Editor Manager enum
    switch(buildingType)
    {
        case Skyscraper1:
        {
            //bbox offset
            bbox_offset = glm::vec3(-7.f, -10.f, 0.f);
            size = glm::vec3(10.f, 20.f, 10.f);
            position = glm::vec3(position.x, 5.f, position.z);
            mesh = [[MeshManager sharedManager] meshForName:@"skyscraper1.obj"];
            [mesh loadWithShader:shader];
            texture = [[TextureManager sharedManager] textureForName:@"skyscraper1.png"];
            break;
        }
        case Skyscraper2:
        {
            //bbox offset
            bbox_offset = glm::vec3(-5.f, 0.f, -5.f);
            size = glm::vec3(10.f, 10.f, 10.f);
            position = glm::vec3(position.x, 5.f, position.z);
            mesh = [[MeshManager sharedManager] meshForName:@"skyscraper2.obj"];
            [mesh loadWithShader:shader];
            texture = [[TextureManager sharedManager] textureForName:@"skyscraper2.png"];
            break;
        }
        case Skyscraper3:
        {
            //bbox offset
            bbox_offset = glm::vec3(0.f, -10.f, -8.f);
            size = glm::vec3(10.f, 21.5f, 10.f);
            position = glm::vec3(position.x, 6.f, position.z);
            mesh = [[MeshManager sharedManager] meshForName:@"skyscraper3.obj"];
            [mesh loadWithShader:shader];
            texture = [[TextureManager sharedManager] textureForName:@"skyscraper3.png"];
            break;
        }
        case Skyscraper4:
        {
            //bbox offset
            bbox_offset = glm::vec3(-7.f, 0.f, -7.f);
            size = glm::vec3(10.f, 10.0f, 10.f);
            position = glm::vec3(position.x, 5.f, position.z);
            mesh = [[MeshManager sharedManager] meshForName:@"skyscraper4.obj"];
            [mesh loadWithShader:shader];
            texture = [[TextureManager sharedManager] textureForName:@"skyscraper4.png"];
            break;
        }
    }
}

#pragma mark -- NSCoding protocol methods
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:buildingType forKey:@"buildingType"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        buildingType = [aDecoder decodeIntForKey:@"buildingType"];
        [self setBuildingType:buildingType];
    }
    return self;
}

- (void)update
{
}

- (void)picking
{    
    [self renderSelected:YES];
}

- (void)render
{
    [self renderSelected:NO];
}

- (void)renderSelected:(BOOL)isSelected
{
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, 90.f, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    //calculate normal matrix
    glm::mat4 modelViewMatrix = [[CameraManager sharedManager] viewMatrix] * modelMatrix;
    normalMatrix = glm::transpose(glm::inverse(glm::mat3x3(modelViewMatrix)));
    
    // update bounding box
    [self updateBBox];
    
    [[ShaderManager sharedManager] useShader:@"TestShader"];
    
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix3fv([shader uniformLocation:@"normalMatrix"], 1, GL_FALSE, &normalMatrix[0][0]);
    glUniform1fv([shader uniformLocation:@"camPos"], 3, glm::value_ptr([[CameraManager sharedManager] position]));
    
    GLfloat renderSelect = (isSelected == YES) ? 1.f : 0.f;
    glUniform1fv([shader uniformLocation:@"renderSelect"], 1, &renderSelect);
    glUniform3fv([shader uniformLocation:@"colorID"], 1, glm::value_ptr(colorID));
    
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
}

#pragma mark - bounding box methods

- (void)renderBBox
{
    [bbox render];
}

- (void)updateBBox
{
    [bbox setSize:(size + bbox_offset)];
    [bbox setRotation:rotation];
    [bbox setPosition:position];
}

@end
