//
//  Tunnel.m
//  Flying Anarchist
//
//  Created by Juan Revuelta on 4/30/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import "Tunnel.h"

@implementation Tunnel
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setupTunnel];
    }
    return self;
}

- (void)setupTunnel
{
    objectType = TUNNEL;
    modelMatrix = glm::mat4(1.f);
    shader = [[ShaderManager sharedManager] createShader:@"SpawnArrow" withFile:@"SpawnArrow.plist"];
    
    mesh = [[MeshManager sharedManager] meshForName:@"tunnel.obj"];
    [mesh loadWithShader:shader];
    
    texture = [[TextureManager sharedManager] textureForName:@"tunnel.png"];
    
    // bbox initialization
    bboxList = [[NSMutableArray alloc] init];
    [bboxList addObject:[[BoundingBox alloc] init]];
    [bboxList addObject:[[BoundingBox alloc] init]];
    [bboxList addObject:[[BoundingBox alloc] init]];
    bboxType = BBOX_LIST;
    
    bbox = [[BoundingBox alloc] init];
}

#pragma mark -- NSCoding protocol methods
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setupTunnel];
    }
    return self;
}

- (void)update
{
    
}

- (void)render
{
    //translation
    modelMatrix = glm::translate(glm::mat4(1.f), position);
    
    //Rotation
    modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
    modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
    modelMatrix = glm::rotate(modelMatrix, 90.f, glm::vec3(0.f, 1.f, 0.f));
    
    //scale
    modelMatrix = glm::scale(modelMatrix, size);
    
    // update bounding box
    [self updateBBox];
    
    //Use shader
    [[ShaderManager sharedManager] useShader:@"SpawnArrow"];
    
    //Pass values to shader
    glUniformMatrix4fv([shader uniformLocation:@"projectionMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] projectionMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"viewMatrix"], 1, GL_FALSE, glm::value_ptr([[CameraManager sharedManager] viewMatrix]));
    glUniformMatrix4fv([shader uniformLocation:@"modelMatrix"], 1, GL_FALSE, &modelMatrix[0][0]);
    
    //Pass texture to shader
    [texture loadTextureToShader:shader usingName:@"s2d_texture"];
    
    [mesh render];
}

#pragma mark - BBox methods

- (void)updateBBox
{
    //[bbox setSize:(size)];
    //[bbox setRotation:rotation];
    //[bbox setPosition:position + glm::vec3(0.f, 2.5f, 0.f)];
    
    //set the values for the multiple bounding boxes
    //bbox 1 top box
    //bbox 2 left box
    //bbox 3 right box
    
    BoundingBox *bbox1 = bboxList[0];
    BoundingBox *bbox2 = bboxList[1];
    BoundingBox *bbox3 = bboxList[2];
    
    // TODO: fix issue with incorrect rotation when using multiple bounding boxes
    [bbox1 setRotation:rotation];
    [bbox2 setRotation:rotation];
    [bbox3 setRotation:rotation];
    
    [bbox1 setPosition:position + glm::vec3(0.f, 2.5f, 0.f)];
    [bbox2 setPosition:position + glm::vec3(-6.f, 0.f, 0.f)];
    [bbox3 setPosition:position + glm::vec3(6.f, 0.f, 0.f)];
    
    [bbox1 setSize:glm::vec3(15.f, 2.5f, 20.f)];
    [bbox2 setSize:glm::vec3(3.f, 5, 20.f)];
    [bbox3 setSize:glm::vec3(3.f, 5, 20.f)];
    
    // bbox for picking
    [bbox setSize:glm::vec3(15.f, 5.f, 20.f)];
    [bbox setRotation:rotation];
    [bbox setPosition:position + glm::vec3(0.f, 0.5f, 0.f)];
}

- (void)renderBBox
{
    [bbox render];
    //[bbox render];
    //printf("bboxList size: %d\n", [bboxList count]);
    //for (BoundingBox *b in bboxList)
    //{
    //    printf("box position: %f %f %f\n", b.position.x, b.position.y, b.position.z);
    //    [b render];
    //}
}
@end
