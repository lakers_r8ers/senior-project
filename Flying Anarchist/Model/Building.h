//
//  Building.h
//  Flying Anarchist
//
//  Created by Lion User on 09/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "GameObject.h"
#import "glm.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "TextureManager.h"
#import "TextureUtil.h"
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "CameraManager.h"
#import "CheckGLErrors.h"
#import "EditorManager.h"
#import "BoundingBox.h"

enum BuildingType
{
    Skyscraper1, Skyscraper2, Skyscraper3, Skyscraper4
};

@class BoundingBox;

@interface Building : GameObject
<NSCoding>
{
    glm::mat4 modelMatrix;
    glm::mat3 normalMatrix;
    
    MeshUtil *mesh;
    ShaderUtil *shader;
    ShaderUtil *selectionShader;
    TextureUtil *texture;
    int buildingType;
    
    //test framebuffer
    GLuint fboID;
    
    // bbox offset size
    glm::vec3 oldSize;
    glm::vec3 bbox_offset;
}

- (void)update;
- (id)initWithBuildingType:(int)type;

@end
