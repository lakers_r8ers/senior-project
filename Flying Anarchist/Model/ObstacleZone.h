//
//  ObstacleZone.h
//  Level Editor
//
//  Created by Lion User on 04/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "glm.hpp"
#import "GameObject.h"
#import "Ground.h"
#import "Skybox.h"
#import "Building.h"
#import "VFCulling.h"

enum MODE
{
    EDITING, GAME
};

@class Ground;
@class Skybox;
@class Building;

@interface ObstacleZone : GameObject
<NSCoding>
{
    NSString *name;
    NSMutableArray *objectsArray;
    int mode;
    Ground *ground;
    
}

@property (nonatomic) NSString *name;
@property (nonatomic) NSMutableArray *objectsArray;
@property (nonatomic) Ground *ground;
@property (nonatomic) int mode;

- (void)render;
- (void)update;

- (void)addGameObject:(GameObject*)object;
- (void)deleteGameObject:(GameObject*)object;
- (void)changeGroundToType:(int)type;

- (void)setZOffset:(float)offset;
 
@end
