//
//  Player.h
//  Level Editor
//
//  Created by Lion User on 04/12/2012.
//  Copyright (c) 2012 Juan Revuelta. All rights reserved.
//

#import "GameObject.h"
#import "glm.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "TextureManager.h"
#import "TextureUtil.h"
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "CameraManager.h"
#import "CheckGLErrors.h"
#import "EditorManager.h"
#import "BoundingBox.h"
#import "GyroManager.h"

#define MAX_HEIGHT 10.f
#define MIN_HEIGHT 1.f
#define STABLE_HEIGHT 5.f

enum
{
  ALIVE, DEAD, MOVING_UP, MOVING_DOWN, STABLE
};

@interface Player : GameObject
{
    float speed;
    int health;
    
    glm::mat4 modelMatrix;
    glm::mat3 normalMatrix;
    
    MeshUtil *mesh;
    ShaderUtil *shader;
    TextureUtil *texture;
    
    glm::vec3 bboxOffset;
    
    // animations
    int animationState;
}

@property (nonatomic, assign) float speed;
@property (nonatomic, assign) int health;
@property (nonatomic, assign) int animationState;

- (void)update;
- (void)setPlayerHeight:(float)height;
- (void)initializePlayer;

@end
