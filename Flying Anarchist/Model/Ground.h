//
//  Ground.h
//  Flying Anarchist
//
//  Created by Lion User on 16/01/2013.
//  Copyright (c) 2013 Juan Revuelta. All rights reserved.
//

#import "GameObject.h"
#import "glm.hpp"
#import "ShaderManager.h"
#import "ShaderUtil.h"
#import "MeshManager.h"
#import "MeshUtil.h"
#import "TextureManager.h"
#import "TextureUtil.h"
#import "glm.hpp"
#import "matrix_transform.hpp"
#import "matrix_inverse.hpp"
#import "type_ptr.hpp"
#import "CameraManager.h"
#import "CheckGLErrors.h"

enum GROUND_TYPE
{
    GROUND1, GROUND2, GROUND3, GROUND4
};

@interface Ground : GameObject
<NSCoding>
{
    glm::mat4 modelMatrix;
    glm::mat3 normalMatrix;
    
    TextureUtil *texture;
    MeshUtil *mesh;
    ShaderUtil *shader;
    int groundType;
}

- (id)initWithGroundType:(int)type;
- (void)changeGroundToType:(int)type;
- (void)update;
@end
